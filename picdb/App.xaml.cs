using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using PicDb.BusinessLogic;
using PicDb.ViewModels;
using PicDb.Views;
using Splat;

namespace PicDb
{
    public class App : Application
    {
        private FileSystemManager _fileSystemManager;
        private PictureBusinessLogic _pictureBusinessLogic;
        private PhotographerBusinessLogic _photographerBusinessLogic;
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainWindow();
                Locator.CurrentMutable.RegisterConstant(desktop.MainWindow, typeof(Window));
                desktop.MainWindow.DataContext = new MainWindowViewModel(desktop.MainWindow);
                _fileSystemManager = Locator.Current.GetService<FileSystemManager>();
                _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
                _pictureBusinessLogic.Initialize();
                _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();

                if (!String.IsNullOrEmpty(_fileSystemManager.CurrentDirectory))
                    desktop.MainWindow.Title = $"Pic DB - {_fileSystemManager.CurrentDirectory}";
                
                _fileSystemManager.DirectoryChanged += (e, title) => desktop.MainWindow.Title = $"Pic DB - {title}";
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}