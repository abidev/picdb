using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels.Dialogs;

namespace PicDb.Dialogs
{
    public class DeleteConfirmationWindow : ReactiveWindow<DeleteConfirmationWindowViewModel>
    {
        public DeleteConfirmationWindow()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}