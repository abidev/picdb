﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Animation;
using Avalonia.Controls;
using Avalonia.Controls.Platform;
using PicDb.BusinessLogic;
using PicDb.Views;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class MainWindowViewModel : ReactiveObject, IScreen, IEnableLogger
    {
        public RoutingState Router { get; } = new RoutingState();

        private readonly Window _window;
        
        public ReactiveCommand<Unit, IRoutableViewModel> LoadPictureView { get; }
        public ReactiveCommand<Unit, IRoutableViewModel> LoadPhotographerView { get; }
        
        public ReactiveCommand<Unit, Unit> OpenDirectoryCommand { get; }
        public ReactiveCommand<string, Unit> PrintTagReportCommand { get; set; }

        public IPageTransition PageTransition { get; } = new CrossFade(TimeSpan.FromSeconds(0.001));
        private readonly PictureBusinessLogic _pictureBusinessLogic;
        
        public PictureViewModel PictureViewModel { get; }
        public PhotographerViewModel PhotographerViewModel { get; }

        public MainWindowViewModel(Window window)
        {
            _window = window;
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
            
            PictureViewModel = new PictureViewModel(this);
            PhotographerViewModel = new PhotographerViewModel(this);
            
            LoadPictureView = ReactiveCommand.CreateFromObservable(
                () => Router.Navigate.Execute(new PictureViewModel(this))
            );
            LoadPhotographerView = ReactiveCommand.CreateFromObservable(
                () => Router.Navigate.Execute(new PhotographerViewModel(this))
            );
            OpenDirectoryCommand = ReactiveCommand.CreateFromTask(OnOpenDirectoryCommand);
            PrintTagReportCommand = ReactiveCommand.Create<string>(OnPrintTagReportCommand);
        }

        private void OnPrintTagReportCommand(string commandArgs)
        {
            switch (commandArgs)
            { 
                case "all":
                    _pictureBusinessLogic.PrintTagInfo(true, TagInfoPrintType.AllPictures);
                    break; 
                case "current":
                    _pictureBusinessLogic.PrintTagInfo(true, TagInfoPrintType.CurrentPicturesInDirectory);
                    break;
                case "this":
                    _pictureBusinessLogic.PrintTagInfo(true, TagInfoPrintType.ThisPicture);
                    break;
                default:
                    _pictureBusinessLogic.PrintTagInfo(true, TagInfoPrintType.AllPictures);
                    break;
            }
        }


        private async Task OnOpenDirectoryCommand()
        {
            var dialog = new OpenFolderDialog();
            var path = await dialog.ShowAsync(_window);
            _pictureBusinessLogic.ParseDirectory(path);
        }
    }
}
