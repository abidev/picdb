using System;
using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Animation;
using PicDb.BusinessLogic;
using PicDb.Data;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class PhotographerViewModel : ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment { get; } = "photographers";
        public IScreen HostScreen { get; }

        private ReactiveObject _content = new ShowPhotographerViewModel();
        public ReactiveObject Content
        {
            get => _content;
            set => this.RaiseAndSetIfChanged(ref _content, value);
        }

        public ReactiveCommand<Unit, Unit> OnCreatePhotographerClickCmd { get; }
        
        public IPageTransition Transition { get; } = new CrossFade(TimeSpan.FromSeconds(0.001));

        private readonly PhotographerBusinessLogic _photographerBusinessLogic;

        public PhotographerViewModel(IScreen screen)
        {
            HostScreen = screen;

            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();
            _photographerBusinessLogic.PhotographerAdded += OnAddPhotographer;
            _photographerBusinessLogic.PhotographerToShowChanged += OnSelectPhotographer;
            _photographerBusinessLogic.PhotographerEditClick += OnEditPhotographerClick;
            _photographerBusinessLogic.PhotographerUpdated += OnUpdatePhotographer;
            

            OnCreatePhotographerClickCmd = ReactiveCommand.CreateFromTask(OnCreatePhotographerClick);
        }

        private async Task OnCreatePhotographerClick()
        {
            ChangeView(typeof(CreatePhotographerViewModel));
        }

        private void OnAddPhotographer(object sender, PhotographerEventArgs args)
        {
            ChangeView(typeof(ShowPhotographerViewModel));
        }

        private void OnUpdatePhotographer(object sender, PhotographerEventArgs args)
        {
            ChangeView(typeof(ShowPhotographerViewModel));
        }

        private void OnEditPhotographerClick(object sender, PhotographerEventArgs args)
        {
            ChangeView(typeof(EditPhotographerViewModel));
        } 

        private void OnSelectPhotographer(object sender, PhotographerEventArgs args)
        {
            ChangeView(typeof(ShowPhotographerViewModel));
        }

        private void ChangeView(Type viewType)
        {
            if (Content.GetType() == viewType) return;

            Content = (ReactiveObject) Activator.CreateInstance(viewType);
        }
    }
}