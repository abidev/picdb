using System;
using System.Reactive;
using PicDb.BusinessLogic;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class EditPhotographerViewModel : ReactiveObject
    {
        public DateTime Today { get; } = DateTime.Today;

        private string _firstName;

        public string FirstName
        {
            get => _firstName;
            set
            {
                this.RaiseAndSetIfChanged(ref _firstName, value);
                ProcessInputChange();
            }
        }
        
        private string _lastName;

        public string LastName
        {
            get => _lastName;
            set
            {
                this.RaiseAndSetIfChanged(ref _lastName, value);
                ProcessInputChange();
            }
        }

        private DateTime _birthdate = DateTime.Today;

        public DateTime Birthdate
        {
            get => _birthdate;
            set
            {
                this.RaiseAndSetIfChanged(ref _birthdate, value);
                ProcessInputChange();
            }
        }

        private string _notes;

        public string Notes
        {
            get => _notes;
            set
            {
                this.RaiseAndSetIfChanged(ref _notes, value);
                ProcessInputChange();
            }
        }

        private bool _isValid = false;

        public bool IsValid
        {
            get => _isValid;
            set => this.RaiseAndSetIfChanged(ref _isValid, value);
        }

        public ReactiveCommand<Unit, Unit> OnSaveClickCmd { get; }

        private readonly PhotographerBusinessLogic _photographerBusinessLogic;

        private Photographer OriginalPhotographer { get; }

        public EditPhotographerViewModel()
        {
            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();
            // Save the original state of the photographer
            OriginalPhotographer = _photographerBusinessLogic.PhotographerToShow;

            // Initialize properties from original photographer
            if (OriginalPhotographer != null)
            {
                FirstName = OriginalPhotographer.FirstName;
                LastName = OriginalPhotographer.LastName;
                Birthdate = OriginalPhotographer.Birthdate;
                Notes = OriginalPhotographer.Notes;
            }

            OnSaveClickCmd = ReactiveCommand.Create(OnSaveClick);
        }

        /// <summary>
        /// Validates input fields and updates IsValid property accordingly.
        /// </summary>
        private void ProcessInputChange()
        {
            if (String.IsNullOrEmpty(FirstName) ||
                String.IsNullOrEmpty(LastName) ||
                Birthdate == DateTime.Today ||
                String.IsNullOrEmpty(Notes))
            {
                IsValid = false;
                return;
            }

            IsValid = true;
        }

        private void OnSaveClick()
        {
            if (!IsValid) return;

            var success = _photographerBusinessLogic.Update(new Photographer
            {
                Id = OriginalPhotographer.Id,
                FirstName = FirstName,
                LastName = LastName,
                Birthdate = Birthdate,
                Notes = Notes
            });

            if (success) this.Log().Info("Successfully edited photographer.");
            else this.Log().Info("Error editing photographer (could be a validation error).");
        }
    }
}