using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using PicDb.BusinessLogic;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class ListPhotographersViewModel : ReactiveObject
    {
        private readonly PhotographerBusinessLogic _photographerBusinessLogic;
        
        public ObservableCollection<Photographer> Photographers { get; }
        
        public ReactiveCommand<int, Unit> OnPhotographerSelectCmd { get; }

        public ListPhotographersViewModel()
        {
            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();
            _photographerBusinessLogic.PhotographerAdded += OnPhotographerAdded;
            _photographerBusinessLogic.PhotographerUpdated += OnPhotographerUpdated;
            _photographerBusinessLogic.PhotographerDeleted += OnPhotographerDeleted;
            
            OnPhotographerSelectCmd = ReactiveCommand.Create<int>(OnPhotographerSelected);

            Photographers = new ObservableCollection<Photographer>(_photographerBusinessLogic.GetAll());
        }

        /// <summary>
        /// Adds new photographers to local list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnPhotographerAdded(object sender, PhotographerEventArgs args)
        {
            if (args?.Photographer == null) return;
            
            Photographers.Add(args.Photographer);
        }

        /// <summary>
        /// Updates local photographer list entry on photographer update.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnPhotographerUpdated(object sender, PhotographerEventArgs args)
        {
            if (args?.Photographer == null) return;

            var index = Photographers.IndexOf(item: Photographers.FirstOrDefault(p => p.Id == args.Photographer.Id));
            
            Photographers[index] = args.Photographer;
        }

        /// <summary>
        /// Removes deleted photographer from local list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnPhotographerDeleted(object sender, PhotographerEventArgs args)
        {
            if (args?.Photographer == null) return;

            var index = Photographers.IndexOf(item: Photographers.FirstOrDefault(p => p.Id == args.Photographer.Id));
            
            Photographers.RemoveAt(index);
        }

        private void OnPhotographerSelected(int photographerId)
        {
           _photographerBusinessLogic.ChangePhotographerToShow(photographerId);
        }
    }
}