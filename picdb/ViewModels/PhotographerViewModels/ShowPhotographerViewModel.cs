using System;
using System.Reactive;
using Avalonia;
using Avalonia.Controls;
using PicDb.BusinessLogic;
using PicDb.Dialogs;
using PicDb.Models;
using PicDb.ViewModels.Dialogs;
using PicDb.Views;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class ShowPhotographerViewModel : ReactiveObject
    {
        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => this.RaiseAndSetIfChanged(ref _firstName, value);
        }
        
        private string _lastName;
        public string LastName
        {
            get => _lastName;
            set => this.RaiseAndSetIfChanged(ref _lastName, value);
        }
        
        private string _birthdate = DateTime.Today.ToShortDateString();
        public string Birthdate
        {
            get => _birthdate;
            set => this.RaiseAndSetIfChanged(ref _birthdate, value);
        }

        private string _notes;
        public string Notes
        {
            get => _notes;
            set => this.RaiseAndSetIfChanged(ref _notes, value);
        }
        
        private bool _hasData;
        public bool HasData
        {
            get => _hasData;
            set => this.RaiseAndSetIfChanged(ref _hasData, value);
        }
        
        private bool _hasNoData;
        public bool HasNoData
        {
            get => _hasNoData;
            set => this.RaiseAndSetIfChanged(ref _hasNoData, value);
        }
        
        private int Id { get; set; }

        private readonly PhotographerBusinessLogic _photographerBusinessLogic;
        private readonly Window _mainWindow;
        
        public ReactiveCommand<Unit, Unit> OnEditClickCmd { get; }
        public ReactiveCommand<Unit, Unit> OnDeleteClickCmd { get; }
        public ShowPhotographerViewModel()
        {
            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();
            _mainWindow = Locator.Current.GetService<Window>();
            
            _photographerBusinessLogic.PhotographerToShowChanged += OnPhotographerToShowChanged;
            
            UpdatePhotographer(_photographerBusinessLogic.PhotographerToShow);

            OnEditClickCmd = ReactiveCommand.Create(OnEditClick);
            OnDeleteClickCmd = ReactiveCommand.Create(OnDeleteClick);
        }

        private void OnEditClick()
        {
            _photographerBusinessLogic.StartPhotographerEdit();
        }

        private async void OnDeleteClick()
        {
            var dialog = new DeleteConfirmationWindow { WindowStartupLocation = WindowStartupLocation.CenterOwner };
            dialog.DataContext = new DeleteConfirmationWindowViewModel(dialog, $"Delete {FirstName} {LastName}?");

            var result = await dialog.ShowDialog<bool>(_mainWindow);
            if (result)
            {
                _photographerBusinessLogic.Delete(_photographerBusinessLogic.PhotographerToShow);
            }
        }

        private void OnPhotographerToShowChanged(object sender, PhotographerEventArgs args)
        {
            UpdatePhotographer(args.Photographer);
        }

        private void UpdatePhotographer(Photographer photographer)
        {
            HasData = photographer != null;
            HasNoData = photographer == null;
            
            if (photographer != null) Id = photographer.Id;
            FirstName = photographer?.FirstName;
            LastName = photographer?.LastName;
            Birthdate = photographer?.Birthdate.ToShortDateString() ?? DateTime.MinValue.ToShortDateString();
            Notes = photographer?.Notes;
        }
    }
}