using System;
using System.Reactive;
using System.Threading.Tasks;
using PicDb.BusinessLogic;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class CreatePhotographerViewModel : ReactiveObject, IEnableLogger
    {
        public DateTime Today { get; } = DateTime.Today;

        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set { this.RaiseAndSetIfChanged(ref _firstName, value); ProcessInputChange(); }
        }
        
        private string _lastName;
        public string LastName
        {
            get => _lastName;
            set { this.RaiseAndSetIfChanged(ref _lastName, value); ProcessInputChange(); }
        }
        
        private DateTime _birthdate = DateTime.Today;
        public DateTime Birthdate
        {
            get => _birthdate;
            set { this.RaiseAndSetIfChanged(ref _birthdate, value); ProcessInputChange(); }
        }
        
        private string _notes;
        public string Notes
        {
            get => _notes;
            set { this.RaiseAndSetIfChanged(ref _notes, value); ProcessInputChange(); }
        }

        private bool _isValid = false;
        public bool IsValid
        {
            get => _isValid;
            set => this.RaiseAndSetIfChanged(ref _isValid, value);
        }
        
        public ReactiveCommand<Unit, Unit> OnSaveClickCmd { get; }

        private readonly PhotographerBusinessLogic _photographerBusinessLogic;

        public CreatePhotographerViewModel()
        {
            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();
            
            OnSaveClickCmd = ReactiveCommand.Create(OnSaveClick);
        }

        /// <summary>
        /// Validates input fields and updates IsValid property accordingly.
        /// </summary>
        private void ProcessInputChange()
        {
            if (String.IsNullOrEmpty(FirstName) ||
                String.IsNullOrEmpty(LastName) ||
                Birthdate == DateTime.Today ||
                String.IsNullOrEmpty(Notes))
            {
                IsValid = false;
                return;
            }

            IsValid = true;
        }
        
        private void OnSaveClick()
        {
            if (!IsValid) return;

            var success = _photographerBusinessLogic.Add(new Photographer
            {
                FirstName = FirstName,
                LastName = LastName,
                Birthdate = Birthdate,
                Notes = Notes
            });
            
            if (success) this.Log().Info("Successfully added photographer.");
            else this.Log().Info("Error adding photographer (could be a validation error).");
        }
    }
}