using System.Reactive;
using Avalonia.Controls;
using ReactiveUI;

namespace PicDb.ViewModels.Dialogs
{
    public class DeleteConfirmationWindowViewModel : ReactiveObject
    {
        private Window _window;
        
        public string Message { get; private set; }
        
        public ReactiveCommand<Unit, Unit> DeleteClickCmd { get; }
        public ReactiveCommand<Unit, Unit> CancelClickCmd { get; }

        public DeleteConfirmationWindowViewModel(Window window, string message)
        {
            _window = window;
            Message = message;

            DeleteClickCmd = ReactiveCommand.Create(OnDeleteClick);
            CancelClickCmd = ReactiveCommand.Create(OnCancelClick);
        }
        
        private void OnDeleteClick()
        { 
            _window.Close(true);
        }
        
        private void OnCancelClick()
        { 
            _window.Close(false);
        }
    }
}