using System;
using System.Reactive;
using PicDb.BusinessLogic;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels.TagsViewModels
{
    public class ShowPictureInfoViewModel: ReactiveObject
    {
        private string _tags = "";
        public string Tags
        {
            get => _tags;
            set => this.RaiseAndSetIfChanged(ref _tags, value);
        }
        
        private string _photographerName = "";
        public string PhotographerName
        {
            get => _photographerName;
            set => this.RaiseAndSetIfChanged(ref _photographerName, value);
        }

        public ReactiveCommand<Unit, Unit> EditClickCommand { get; }
        public ReactiveCommand<Unit, Unit> PrintPictureInfoCommand { get;  }

        private readonly PictureBusinessLogic _pictureBusinessLogic;
        private readonly PhotographerBusinessLogic _photographerBusinessLogic;
        

        public ShowPictureInfoViewModel()
        {
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();

            _pictureBusinessLogic.PictureToShowChanged += OnPictureToShowChanged;

            EditClickCommand = ReactiveCommand.Create(OnEditClick);
           
            // Initialize to the currently selected picture
            PrintPictureInfoCommand = ReactiveCommand.Create(OnPrintPictureInfoClick);
            
            UpdateTags(_pictureBusinessLogic.PictureToShow);
        }

        private void OnPrintPictureInfoClick()
        {
            var picToShow = _pictureBusinessLogic.PictureToShow;
            picToShow.Photographer = _photographerBusinessLogic.Get(picToShow.PhotographerId);
            _pictureBusinessLogic.PrintPictureInfo(picToShow);
        }

        private void OnEditClick()
        {
            _pictureBusinessLogic.StartPictureInfoEdit();
        }

        private void OnPictureToShowChanged(object sender, PictureEventArgs args)
        {
            if (args.Picture == null) return;
            
            UpdateTags(args.Picture);
        }

        private void UpdateTags(Picture picture)
        {
            if (picture == null) return;
            
            Tags = picture.Tags;
            if (picture.PhotographerId > 0)
            {
                var photographer = _photographerBusinessLogic.Get(picture.PhotographerId);
                PhotographerName = $"{photographer.FirstName} {photographer.LastName}";
            }
            else
            {
                PhotographerName = "";
            }
        }
    }
}