using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using PicDb.BusinessLogic;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels.TagsViewModels
{
    public class EditPictureInfoViewModel: ReactiveObject
    {
        private string _tags = "";
        public string Tags
        {
            get => _tags;
            set => this.RaiseAndSetIfChanged(ref _tags, value);
        }

        public ReactiveCommand<Unit, Unit> SaveClickCommand { get; }

        private readonly PictureBusinessLogic _pictureBusinessLogic;
        private readonly PhotographerBusinessLogic _photographerBusinessLogic;

        private readonly Picture _picture;
        public ObservableCollection<PhotographerOption> Photographers { get; }
        private PhotographerOption _selectedPhotographerOption;
        public PhotographerOption SelectedPhotographerOption
        {
            get => _selectedPhotographerOption;
            set => this.RaiseAndSetIfChanged(ref _selectedPhotographerOption, value);
        } 

        public EditPictureInfoViewModel()
        {
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();

            SaveClickCommand = ReactiveCommand.Create(OnSaveClick);

            _picture = _pictureBusinessLogic.PictureToShow;
            Photographers = new ObservableCollection<PhotographerOption>(_photographerBusinessLogic.GetAll().Select(p => new PhotographerOption(p)));
            
            // Preselect previous photographer
            SelectedPhotographerOption = Photographers.FirstOrDefault(p => p.Photographer.Id == _picture.PhotographerId);
            
            // Adds an empty entry used to deselect the photographers dropdown
            Photographers.Add(new PhotographerOption(new Photographer
            {
                Id = 0,
                FirstName = "",
                LastName = ""
            }));
            Tags = _picture.Tags;
        }

        private void OnSaveClick()
        {
            var tagList = Tags.Split(';').Select(x => x.Trim()).ToArray().Distinct().ToList();
            var newTag = string.Empty;
            
            foreach (var tag in tagList)
            {
                var trimedTag = tag.Trim();
                
                if(trimedTag != string.Empty)
                    newTag += $"{trimedTag}; ";
            }

            _picture.Tags = newTag;
            _picture.PhotographerId = SelectedPhotographerOption.Photographer.Id;

            _pictureBusinessLogic.Update(_picture);
        }
    }

    /// <summary>
    /// Provides helpers for displaying photographer dropdown options.
    /// </summary>
    public class PhotographerOption
    {
        public Photographer Photographer { get; }

        public bool IsValid { get; }
        public bool IsNotValid { get; }
        
        public string FullName { get; }

        public PhotographerOption(Photographer photographer)
        {
            Photographer = photographer;
            FullName = $"{Photographer?.FirstName} {Photographer?.LastName}";
            IsValid = Photographer?.Id > 0;
            IsNotValid = Photographer?.Id <= 0;
        }
    }
}