using System;
using System.IO;
using System.Linq;
using Avalonia;
using Avalonia.Media.Imaging;
using Avalonia.Visuals.Media.Imaging;
using PicDb.BusinessLogic;
using PicDb.Data;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class PictureLargeViewModel : ViewModelBase
    {
        private readonly PictureBusinessLogic _pictureBusinessLogic;
        private Bitmap _imageSource;
        private Bitmap ImageSource { 
            get => _imageSource;
            set => this.RaiseAndSetIfChanged(ref _imageSource, value);
        }
        
        public PictureLargeViewModel()
        {
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
            _pictureBusinessLogic.PictureToShowChanged += OnPictureToShowChanged;
            
            ImageSource = _pictureBusinessLogic.GetPictureBitmap(_pictureBusinessLogic.PictureToShow);
        }
        
        private void OnPictureToShowChanged(object sender, PictureEventArgs e)
        {
            if (e?.Picture == null) return;

            ImageSource = _pictureBusinessLogic.GetPictureBitmap(e.Picture);
        }
    }
}