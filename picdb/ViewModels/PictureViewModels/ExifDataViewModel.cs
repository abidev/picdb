﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reactive;
using PicDb.BusinessLogic;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class ExifDataViewModel : ReactiveObject
    {
        public DateTime Today { get; } = DateTime.Today;
        
        private readonly PictureBusinessLogic _pictureBusinessLogic;

        private bool _hasExifData;

        private bool _hasNoExifData;

        private bool _isValid;
        
        private bool _selectionChanged;

        private bool _isDatePickerVisible;

        private bool _isTextBoxVisible;

        private ExifDataEdit _selectedExifItem;

        private string _title;

        private string _value;

        private DateTime _dateTimeValue;
        
        private ICollection<string> ExcludedTags;

        public ExifDataViewModel()
        {
            HasExifData = false;
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
            OnSaveClickCmd = ReactiveCommand.Create(OnSaveClick);
            _pictureBusinessLogic.PictureToShowChanged += OnPictureToShowChanged;
            IsDatePickerVisible = false;
            IsTextBoxVisible = false;
            ExifDataEdits = new ObservableCollection<ExifDataEdit>();
            ExcludedTags = new List<string> {"Id", "ExifVersion"};
            var picToShow = _pictureBusinessLogic.PictureToShow;
            if (picToShow != null)
            {
                OriginalExifData = picToShow.ExifData;
                EditedExifData = OriginalExifData;
                ExifDataEdits.Clear();
                
                // Initialize to previous exif data
                InitializeExifSelection(OriginalExifData);
            }
        }

        private ObservableCollection<ExifDataEdit> ExifDataEdits { get; }

        public string Title
        {
            get => _title;
            set => this.RaiseAndSetIfChanged(ref _title, value);
        }

        public string Value
        {
            get => _value;
            set
            {
                this.RaiseAndSetIfChanged(ref _value, value);
                ProcessInputChange();
            }
        }

        public DateTime DateTimeValue
        {
            get => _dateTimeValue;
            set
            {
                this.RaiseAndSetIfChanged(ref _dateTimeValue, value);
                ProcessInputChange();
            }
        }

        public ExifDataEdit SelectedExifItem
        {
            get => _selectedExifItem;
            set
            {
                this.RaiseAndSetIfChanged(ref _selectedExifItem, value);
                if (value != null)
                {
                    _selectionChanged = true;
                    Title = value.Title;
                    Value = value.Value;
                    ProcessSelection();
                }
            }
        }

        public bool IsValid
        {
            get => _isValid;
            set => this.RaiseAndSetIfChanged(ref _isValid, value);
        }

        public bool HasExifData
        {
            get => _hasExifData;
            set
            {
                this.RaiseAndSetIfChanged(ref _hasExifData, value);
                HasNoExifData = !value;
            }
        }

        public bool HasNoExifData
        {
            get => _hasNoExifData;
            set => this.RaiseAndSetIfChanged(ref _hasNoExifData, value);
        }

        public bool IsDatePickerVisible
        {
            get => _isDatePickerVisible;
            set => this.RaiseAndSetIfChanged(ref _isDatePickerVisible, value);
        }
        
        public bool IsTextBoxVisible
        {
            get => _isTextBoxVisible;
            set => this.RaiseAndSetIfChanged(ref _isTextBoxVisible, value);
        }

        public ReactiveCommand<Unit, Unit> OnSaveClickCmd { get; }

        private ExifData OriginalExifData { get; set; }
        private ExifData EditedExifData { get; set; }

        private void ProcessSelection()
        {
            _selectionChanged = true;
            
            switch (Title)
            {
                case "DateTimeOriginal":
                    DateTimeValue = Convert.ToDateTime(EditedExifData.GetType().GetProperty(Title)?.GetValue(EditedExifData)?.ToString());
                    IsTextBoxVisible = false;
                    IsDatePickerVisible = true;
                    break;
                default:
                    Value = EditedExifData.GetType().GetProperty(Title)?.GetValue(EditedExifData)?.ToString();
                    IsDatePickerVisible = false;
                    IsTextBoxVisible = true;
                    break;
            }
        }

        private void ProcessInputChange()
        {
            if (!_selectionChanged)
            {
                switch (Title)
                {
                    case "DateTimeOriginal":
                        EditedExifData.GetType().GetProperty(Title)?.SetValue(EditedExifData, DateTimeValue);
                        break;
                    default:
                        EditedExifData.GetType().GetProperty(Title)?.SetValue(EditedExifData, Value);
                        break;
                }
            }
            _selectionChanged = false;
            
            //check if string is empty
            if (String.IsNullOrWhiteSpace(Value) &&
                OriginalExifData.GetType().GetProperty(Title)?.GetValue(OriginalExifData)?.ToString() != Value)
            {
                //check if string has changed
                IsValid = false;
                return;
            }
            IsValid = true;
        }

        private void OnSaveClick()
        {
            if (!IsValid) return;

            var dateTimeVar = OriginalExifData.DateTimeOriginal;

            if (EditedExifData.DateTimeOriginal != OriginalExifData.DateTimeOriginal)
                dateTimeVar = EditedExifData.DateTimeOriginal;

            // Update with edited values, use original value if nothing changed
            var success = _pictureBusinessLogic.UpdateExifData(new ExifData
            {
                Id = OriginalExifData.Id,
                Make = EditedExifData.Make ?? OriginalExifData.Make,
                Model = EditedExifData.Model ?? OriginalExifData.Model,
                Orientation = EditedExifData.Orientation ?? OriginalExifData.Orientation,
                DateTimeOriginal = dateTimeVar
            });

            if (success)
            {
                this.Log().Info("Successfully edited EXIF Data with ID = {0}.", OriginalExifData.Id);
                OriginalExifData = EditedExifData;
                ProcessInputChange();
            }
            else
            {
                this.Log().Info("Error editing photographer (could be a validation error).");
            }
        }

        /// <summary>
        /// Resets data on selected image change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPictureToShowChanged(object sender, PictureEventArgs e)
        {
            if (e?.Picture == null) return;

            OriginalExifData = e.Picture.ExifData;
            EditedExifData = OriginalExifData;
            ExifDataEdits.Clear();
            InitializeExifSelection(OriginalExifData);
        }

        private void InitializeExifSelection(ExifData originalExifData)
        {
            foreach (var property in originalExifData.GetType().GetProperties())
            {
                var propertyName = property.Name;
                if (!ExcludedTags.Contains(propertyName))
                {
                    //if Title doesn't exist => add it to the list.
                    if (ExifDataEdits.Count(x => x.Title == propertyName).Equals(0))
                    {
                        if (propertyName != "DateTimeOriginal")
                        {
                            ExifDataEdits.Add(new ExifDataEdit
                            {
                                Title = propertyName,
                                Value = property.GetValue(originalExifData)?.ToString(),
                            });
                        }
                        else
                        {
                            var dateTimeNew = (DateTime) property.GetValue(originalExifData);

                            if (property.GetValue(originalExifData)?.ToString() == null) 
                                dateTimeNew = DateTime.Now;

                            ExifDataEdits.Add(new ExifDataEdit
                            {
                                Title = propertyName,
                                Value = dateTimeNew.ToString(CultureInfo.InvariantCulture)
                            });
                        }
                    }
                    //if Title already exists => change values
                    else
                    {
                        ExifDataEdits.First(x => x.Title == propertyName).Value =
                            property.GetValue(originalExifData)?.ToString();
                    }
                }
            }

            if (ExifDataEdits.Count >= 1)
            {
                HasExifData = true;
                SelectedExifItem = ExifDataEdits.First();
                _selectionChanged = true;
                ProcessInputChange();
            }
            else
            {
                HasExifData = false;
            }
        }
    }

    public class ExifDataEdit : ReactiveObject
    {
        public string Title { get; set; }
        public string Value { get; set; }
    }
}