using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using PicDb.BusinessLogic;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class SearchViewModel: ReactiveObject
    {
        private string _searchString = "";
        public string SearchString
        {
            get => _searchString;
            set
            {
                this.RaiseAndSetIfChanged(ref _searchString, value);
                OnSearchStringChange();
            }
        }

        private bool _showSearchHint = false;

        public bool ShowSearchHint
        {
            get => _showSearchHint;
            set
            {
                this.RaiseAndSetIfChanged(ref _showSearchHint, value);
            }
        }

        public ReactiveCommand<Unit, Unit> ClearSearchCommand { get; }

        private readonly Subject<string> _searchStringSubject = new Subject<string>();

        private readonly PictureBusinessLogic _pictureBusinessLogic;

        public SearchViewModel()
        {
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();

            ClearSearchCommand = ReactiveCommand.Create(OnClearSearch);

            // Debounce search input to prevent search before entire search query has been entered
            _searchStringSubject.Throttle(TimeSpan.FromMilliseconds(300))
                .Subscribe(_ => OnDebouncedSearchStringChange());
        }

        private void OnSearchStringChange()
        {
            _searchStringSubject.OnNext(SearchString);
        }

        private void OnDebouncedSearchStringChange()
        {
            Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(() => _pictureBusinessLogic.Search(SearchString));

            ShowSearchHint = !String.IsNullOrEmpty(SearchString);
        }

        private void OnClearSearch()
        {
            SearchString = "";
            ShowSearchHint = false;
        }
    }
}