﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using PicDb.BusinessLogic;
using PicDb.Models;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class IptcDataViewModel : ReactiveObject
    {
        private readonly PictureBusinessLogic _pictureBusinessLogic;

        private bool _hasIptcData;

        private bool _hasNoIptcData;

        private bool _isValid;

        private bool _selectionChanged;

        private IptcDataEdit _selectedIptcItem;

        private string _title;

        private string _value;

        private ICollection<string> ExcludedTags;

        public IptcDataViewModel()
        {
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
            OnSaveClickCmd = ReactiveCommand.Create(OnSaveClick);
            _pictureBusinessLogic.PictureToShowChanged += OnPictureToShowChanged;
            HasIptcData = false;
            IptcDataEdits = new ObservableCollection<IptcDataEdit>();
            ExcludedTags = new List<string> {"Id", "Width", "Height"};
            var picToShow = _pictureBusinessLogic.PictureToShow;
            if (picToShow != null)
            {
                OriginalIptcData =picToShow.IptcData;
                EditedIptcData = OriginalIptcData;
                IptcDataEdits.Clear();
                InitializeIptcSelection(OriginalIptcData);
            }
        }

        private ObservableCollection<IptcDataEdit> IptcDataEdits { get; }

        public string Title
        {
            get => _title;
            set => this.RaiseAndSetIfChanged(ref _title, value);
        }

        public string Value
        {
            get => _value;
            set
            {
                this.RaiseAndSetIfChanged(ref _value, value);
                ProcessInputChange();
            }
        }

        public IptcDataEdit SelectedIptcItem
        {
            get => _selectedIptcItem;
            set
            {
                this.RaiseAndSetIfChanged(ref _selectedIptcItem, value);
                if (value != null)
                {
                    _selectionChanged = true;
                    Title = value.Title;
                    Value = value.Value;
                    ProcessSelection();
                }
            }
        }

        public bool IsValid
        {
            get => _isValid;
            set => this.RaiseAndSetIfChanged(ref _isValid, value);
        }

        public bool HasIptcData
        {
            get => _hasIptcData;
            set
            {
                this.RaiseAndSetIfChanged(ref _hasIptcData, value);
                HasNoIptcData = !value;
            }
        }

        public bool HasNoIptcData
        {
            get => _hasNoIptcData;
            set => this.RaiseAndSetIfChanged(ref _hasNoIptcData, value);
        }

        public ReactiveCommand<Unit, Unit> OnSaveClickCmd { get; }

        private IptcData OriginalIptcData { get; set; }
        private IptcData EditedIptcData { get; set; }

        private void ProcessSelection()
        {
            Value = EditedIptcData.GetType().GetProperty(Title)?.GetValue(EditedIptcData)?.ToString();
            _selectionChanged = true;
        }

        private void ProcessInputChange()
        {
            if (!_selectionChanged)
            {
                EditedIptcData.GetType().GetProperty(Title)?.SetValue(EditedIptcData, Value);
            }
            _selectionChanged = false;

            //TODO find out where OriginalIptcData is changed ? It's always the same as EditedIptcData => but why ?
            if (string.IsNullOrEmpty(Value) &&
                OriginalIptcData.GetType().GetProperty(Title)?.GetValue(OriginalIptcData)?.ToString() != Value)
            {
                IsValid = false;
                return;
            }
            IsValid = true;
        }

        private void OnSaveClick()
        {
            if (!IsValid) return;

            var success = _pictureBusinessLogic.UpdateIptcData(new IptcData
            {
                Id = OriginalIptcData.Id,
                Title = EditedIptcData.Title ?? OriginalIptcData.Title,
                KeyWords = EditedIptcData.KeyWords ?? OriginalIptcData.KeyWords,
                Width = OriginalIptcData.Width,
                Height = OriginalIptcData.Height,
                Creators = EditedIptcData.Creators ?? OriginalIptcData.Creators,
            });

            if (success)
            {
                this.Log().Info("Successfully edited Iptc Data with ID = {0}.", OriginalIptcData.Id);
                OriginalIptcData = EditedIptcData;
                ProcessInputChange();
            }
            else
            {
                this.Log().Info("Error editing photographer (could be a validation error).");
            }
        }

        /**
         * if a picture is shown in LargePictureView
         */
        private void OnPictureToShowChanged(object sender, PictureEventArgs e)
        {
            if (e?.Picture == null) return;

            OriginalIptcData = e.Picture.IptcData;
            EditedIptcData = OriginalIptcData;
            IptcDataEdits.Clear();
            InitializeIptcSelection(OriginalIptcData);
        }

        private void InitializeIptcSelection(IptcData originalIptcData)
        {
            foreach (var property in originalIptcData.GetType().GetProperties())
            {
                var propertyName = property.Name;
                if (!ExcludedTags.Contains(propertyName))
                {
                    //if Title doesn't exist => add it to the list.
                    if (IptcDataEdits.Where(x => x.Title == propertyName).Count().Equals(0))
                    {
                        IptcDataEdits.Add(new IptcDataEdit
                            {
                                Title = propertyName,
                                Value = property.GetValue(originalIptcData)?.ToString(),
                            });
                    }
                    //if Title already exists => change values
                    else
                    {
                        IptcDataEdits.First(x => x.Title == propertyName).Value =
                            property.GetValue(originalIptcData)?.ToString();
                    }
                }
            }

            if (IptcDataEdits.Count >= 1)
            {
                HasIptcData = true;
                SelectedIptcItem = IptcDataEdits.First();
                _selectionChanged = true;
            }
            else
            {
                HasIptcData = false;
            }
        }
    }

    public class IptcDataEdit : ReactiveObject
    {
        public string Title { get; set; }
        public string Value { get; set; }
    }
}