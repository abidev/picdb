using System;
using Avalonia.Animation;
using PicDb.BusinessLogic;
using PicDb.ViewModels.TagsViewModels;
using ReactiveUI;
using Splat;

namespace PicDb.ViewModels
{
    public class PictureInfoViewModel: ReactiveObject
    {
        private PictureInfoViewModes _currentMode = PictureInfoViewModes.Show;
        private ReactiveObject _content = new ShowPictureInfoViewModel();
        public ReactiveObject Content
        {
            get => _content;
            set => this.RaiseAndSetIfChanged(ref _content, value);
        }
        
        public IPageTransition Transition { get; } = new CrossFade(TimeSpan.FromSeconds(0.001));

        private readonly PictureBusinessLogic _pictureBusinessLogic;

        public PictureInfoViewModel()
        {
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();

            _pictureBusinessLogic.PictureEditInfoClick += OnPictureEditInfoClick;
            _pictureBusinessLogic.PictureToShowChanged += OnPictureToShowChanged;
        }

        private void OnPictureEditInfoClick(object sender, PictureEventArgs args)
        {
            if (_currentMode == PictureInfoViewModes.Edit) return;
            
            ChangeView(PictureInfoViewModes.Edit);
        }
        
        private void OnPictureToShowChanged(object sender, PictureEventArgs args)
        {
            if (_currentMode == PictureInfoViewModes.Show) return;
            
            ChangeView(PictureInfoViewModes.Show);
        }

        /// <summary>
        /// Changes view to fit the specified mode.
        /// </summary>
        /// <param name="mode">View mode</param>
        private void ChangeView(PictureInfoViewModes mode)
        {
            switch (mode)
            {
                case PictureInfoViewModes.Show:
                    Content = new ShowPictureInfoViewModel();
                    _currentMode = PictureInfoViewModes.Show;
                    break;
                case PictureInfoViewModes.Edit:
                    Content = new EditPictureInfoViewModel();
                    _currentMode = PictureInfoViewModes.Edit;
                    break;
            }
        }
    }

    public enum PictureInfoViewModes
    {
        Show,
        Edit
    }
}