﻿using System;
using System.Collections.ObjectModel;
using ReactiveUI;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using Avalonia;
using Avalonia.Input;
using PicDb.BusinessLogic;
using PicDb.Models;
using Splat;

namespace PicDb.ViewModels
{
    public class PictureListViewModel : ReactiveObject
    {
        private ObservableCollection<Thumbnail> Thumbnails { get; }
        private Thumbnail _selectedThumbnail;
        private readonly PictureBusinessLogic _pictureBusinessLogic;

        public ReactiveCommand<int, Unit> OnImageListItemSelectCmd { get; }
        
        public event EventHandler<ThumbnailClickedEventArgs> ThumbnailClicked;

        public PictureListViewModel()
        {
            _pictureBusinessLogic = Locator.Current.GetService<PictureBusinessLogic>();
            _pictureBusinessLogic.ThumbnailsChanged += NotifyThumbnailsChanged;

            Thumbnails = new ObservableCollection<Thumbnail>();
            
            OnImageListItemSelectCmd = ReactiveCommand.Create<int>(OnImageListItemSelected);
            
            InitializeThumbnails(_pictureBusinessLogic.GetCurrentThumbnails());
        }

        /// <summary>
        /// Called when an image changes (created, updated, deleted, directory changed)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotifyThumbnailsChanged(object sender, ThumbnailsChangedEventArgs e)
        {
            switch (e.ChangeType)
            {
                case ChangeType.Deleted:
                    DeleteThumbnails(e.Thumbnails);
                    break;
                case ChangeType.Renamed:
                    // do nothing
                    break;
                case ChangeType.Created:
                    AddThumbnails(e.Thumbnails);
                    break;
                case ChangeType.Changed:
                    ReplaceThumbnails(e.Thumbnails);
                    break;
                case ChangeType.DirectoryChanged:
                    InitializeThumbnails(e.Thumbnails);
                    break;
            }
        }

        private void ReplaceThumbnails(List<Thumbnail> thumbnails)
        {
            foreach (var pic in thumbnails)
            {
                var index = Thumbnails.IndexOf(item: Thumbnails.FirstOrDefault(p => p.Id == pic.Id));
                Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(() => Thumbnails.RemoveAt(index));
                Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(() => Thumbnails.Add(pic));
            }
        }

        private void DeleteThumbnails(IEnumerable<Thumbnail> thumbnails)
        {
            foreach (var pic in thumbnails)
            {
                try
                {
                    var index = Thumbnails.IndexOf(item: Thumbnails.FirstOrDefault(p => p.Id == pic.Id));
                    Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(() => Thumbnails.RemoveAt(index));
                    //Thumbnails.RemoveAt(index);
                }
                catch (FileNotFoundException)
                {
                    //TODO WAIT WHAT ?????
                }
            }
        }

        private void AddThumbnails(IEnumerable<Thumbnail> thumbnails)
        {
            var thumbi = thumbnails;
            foreach (var pic in thumbi)
            {
                if (pic.ThumbnailPic != null)
                {
                    try
                    {
                        Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(() => Thumbnails.Add(pic));
                    }
                    catch (FileNotFoundException)
                    {
                        Console.WriteLine("ID: {0} was not found.", pic.Id);
                    }
                }
            }
        }
        
        public Thumbnail SelectedThumbnail
        {
            get =>_selectedThumbnail;
            set => this.RaiseAndSetIfChanged(ref _selectedThumbnail, value);
        }
        
        public void ResetThumbnails()
        {
           InitializeThumbnails(_pictureBusinessLogic.GetCurrentThumbnails());
        }

        private void InitializeThumbnails(IEnumerable<Thumbnail> thumbnails)
        {
            Thumbnails.Clear();
            if (thumbnails == null) return;
            
            foreach (var pic in thumbnails)
            {
                if (pic.ThumbnailPic != null)
                {
                    try
                    {
                        Thumbnails.Add(pic);
                    }

                    catch (FileNotFoundException)
                    {
                        Console.WriteLine("ID: {0} was not found.", pic.Id);
                    }
                }
            }
            this.Log().Info($"PictureListView initialzied with {Thumbnails.Count} Elements.");
        }
        
        //gets called from ImageListView
        public void OnImageListItemSelected(int thumbnailId)
        {
            _pictureBusinessLogic.ChangeLargeImageToShow(thumbnailId);
        }
        
        protected virtual void OnThumbnailClicked(ThumbnailClickedEventArgs e)
        {
            ThumbnailClicked?.Invoke(this, e);
        }
    }

    public abstract class ThumbnailClickedEventArgs : EventArgs
    {
        public Picture ClickedPic { get; set; }
    }
}