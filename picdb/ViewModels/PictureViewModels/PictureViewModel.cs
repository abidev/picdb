using System;
using ReactiveUI;

namespace PicDb.ViewModels
{
    public class PictureViewModel : ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment { get; } = "pictures";
        public IScreen HostScreen { get; }
        
        private PictureLargeViewModel PictureLargeViewModel { get; }

        public PictureViewModel(IScreen screen)
        {
            HostScreen = screen;
            PictureLargeViewModel = new PictureLargeViewModel();
        }
    }
}