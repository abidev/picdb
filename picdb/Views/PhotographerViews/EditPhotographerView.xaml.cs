using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels;

namespace PicDb.Views
{
    public class EditPhotographerView : ReactiveUserControl<EditPhotographerViewModel>
    {
        public EditPhotographerView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}