using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels;
using ReactiveUI;

namespace PicDb.Views
{
    public class CreatePhotographerView : ReactiveUserControl<CreatePhotographerViewModel>
    {
        public CreatePhotographerView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}