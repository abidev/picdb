using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PicDb.ViewModels;

namespace PicDb.Views
{
    public class ListPhotographersView : UserControl
    {
        public ListPhotographersView()
        {
            InitializeComponent();
            DataContext = new ListPhotographersViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}