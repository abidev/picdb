using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PicDb.ViewModels;

namespace PicDb.Views
{
    public class ExifView : UserControl
    {
        public ExifView()
        {
            InitializeComponent();
            DataContext = new ExifDataViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}