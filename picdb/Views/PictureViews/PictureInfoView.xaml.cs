using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels;
using ReactiveUI;

namespace PicDb.Views
{
    public class PictureInfoView : ReactiveUserControl<PictureInfoViewModel>
    {
        public PictureInfoView()
        {
            InitializeComponent();
            DataContext = new PictureInfoViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}