using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PicDb.ViewModels;

namespace PicDb.Views
{
    public class IptcView : UserControl
    {
        public IptcView()
        {
            InitializeComponent();
            DataContext = new IptcDataViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}