﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Input;
using Avalonia.Layout;
using Avalonia.Markup.Xaml;
using PicDb.Models;
using PicDb.ViewModels;
using SharpDX.Direct2D1;
using Image = Avalonia.Controls.Image;
using Orientation = Avalonia.Layout.Orientation;

namespace PicDb.Views
{
    public class PictureListView : UserControl
    {
        public PictureListView()
        {
            InitializeComponent();
            DataContext = new PictureListViewModel();
        }
        
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}