using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels.TagsViewModels;

namespace PicDb.Views
{
    public class ShowPictureInfoView : ReactiveUserControl<ShowPictureInfoViewModel>
    {
        public ShowPictureInfoView()
        {
            InitializeComponent();
            DataContext = new ShowPictureInfoViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}