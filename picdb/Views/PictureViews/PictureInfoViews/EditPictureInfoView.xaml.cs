using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels.TagsViewModels;

namespace PicDb.Views
{
    public class EditPictureInfoView : ReactiveUserControl<EditPictureInfoViewModel>
    {
        public EditPictureInfoView()
        {
            InitializeComponent();
            DataContext = new EditPictureInfoViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}