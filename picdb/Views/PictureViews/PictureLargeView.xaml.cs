using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels;
using ReactiveUI;

namespace PicDb.Views
{
    public class PictureLargeView : ReactiveUserControl<PictureLargeViewModel>
    {
        public PictureLargeView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.WhenActivated(disposables => { });
            AvaloniaXamlLoader.Load(this);
        }
    }
}