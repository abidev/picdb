using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using PicDb.ViewModels;
using ReactiveUI;

namespace PicDb.Views
{
    public class PhotographerView : ReactiveUserControl<PhotographerViewModel>
    {
        public PhotographerView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.WhenActivated(disposables => { });
            AvaloniaXamlLoader.Load(this);
        }
    }
}