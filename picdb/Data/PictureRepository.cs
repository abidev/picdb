using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Microsoft.Data.Sqlite;
using PicDb.BusinessLogic;
using PicDb.Models;
using Splat;

namespace PicDb.Data
{
    public class PictureRepository : ISearchableRepository<Picture>
    {
        private readonly SqliteConnection _connection;

        public PictureRepository()
        {
            var settings = ConfigurationManager.ConnectionStrings[PicDbConfig.DbConnectionKey];
            var connectionString = settings.ConnectionString;
            _connection = new SqliteConnection(connectionString);
            
            _connection.Open();
        }

        /// <summary>
        ///     Gets Picture from Database with given ID
        /// </summary>
        /// <param name="id">
        ///     ID of Picture that should be fetched from the Database
        /// </param>
        /// <returns>
        ///     Picture element or null if ID doesnt exist.
        /// </returns>
        public Picture Get(int id)
        {
            if (id <= 0) return null;

            Picture result = null;
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "SELECT id, directory, file_name, iptc_id, exif_id, photographer_id, tags FROM picture WHERE id = @id LIMIT 1"
            };
            
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = id, DbType = DbType.Int32 });

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var picture = new Picture
                {
                    Id = reader.GetInt32(0),
                    Directory = reader.GetString(1),
                    FileName = reader.GetString(2),
                    IptcDataId = reader.GetInt32(3),
                    ExifDataId = reader.GetInt32(4),
                    PhotographerId = reader.GetInt32(5),
                    Tags = reader.GetString(6)
                };

                picture.ExifData = FetchExifData(picture.ExifDataId);
                picture.IptcData = FetchIptcData(picture.IptcDataId);

                result = picture;
            }

            reader.Close();
            return result;
        }
        
        /// <summary>
        ///     Gets all entities from Picture Database
        /// </summary>
        /// <returns>
        ///     List of all Pictures that were found in the database
        /// </returns>
        public IEnumerable<Picture> GetAll()
        {
            List<Picture> pictures = new List<Picture>();

            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "SELECT id, directory, file_name, iptc_id, exif_id, photographer_id, tags FROM picture"
            };

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var picture = new Picture
                {
                    Id = reader.GetInt32(0),
                    Directory = reader.GetString(1),
                    FileName = reader.GetString(2),
                    IptcDataId = reader.GetInt32(3),
                    ExifDataId = reader.GetInt32(4),
                    PhotographerId = reader.GetInt32(5),
                    Tags = reader.GetString(6)
                };

                picture.ExifData = FetchExifData(picture.ExifDataId);
                picture.IptcData = FetchIptcData(picture.IptcDataId);
                
                pictures.Add(picture);
            }

            reader.Close();

            return pictures;
        }

        /// <summary>
        ///     Adds given Picture to Database
        /// </summary>
        /// <param name="entity">
        ///     entity of Picture object that should be added.
        /// </param>
        /// <returns>
        ///     Picture Object that was built and added to the Database
        /// </returns>
        public Picture Add(Picture entity)
        {
            using var cmd = new SqliteCommand
            {
                Connection = _connection
            };

            var exifRes = AddOrUpdateExif(entity.ExifData);
            var iptcRes = AddOrUpdateIptc(entity.IptcData);

            cmd.CommandText = "INSERT INTO picture (directory, file_name, exif_id, iptc_id, photographer_id, tags) VALUES (@directory, @file_name, @exif_id, @iptc_id, @photographer_id, @tags)";
            
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "directory", Value = entity.Directory, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "file_name", Value = entity.FileName, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "exif_id", Value = exifRes, DbType = DbType.Int32});
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "iptc_id", Value = iptcRes, DbType = DbType.Int32});
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "photographer_id", Value = entity.PhotographerId, DbType = DbType.Int32});
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "tags", Value = entity.Tags ?? "", DbType = DbType.String});

            cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch

            entity.ExifData.Id = exifRes;
            entity.ExifDataId = exifRes;
            entity.IptcData.Id = iptcRes;
            entity.IptcDataId = iptcRes;
            
            cmd.CommandText = "SELECT last_insert_rowid() FROM exif_data";
            entity.Id =  Convert.ToInt32(cmd.ExecuteScalar());

            return entity;
        }

        /// <summary>
        ///    Updates given picture in Database
        /// </summary>
        /// <param name="entity">
        ///    Picture that should be updated
        /// </param>
        public void Update(Picture entity)
        {
            using var cmd = new SqliteCommand
            {
                Connection = _connection
            };
            
            var exifRes = AddOrUpdateExif(entity.ExifData);
            var exifId = exifRes > 0 ? exifRes : entity.ExifData.Id;
            var iptcRes = AddOrUpdateIptc(entity.IptcData);
            var iptcId = iptcRes > 0 ? iptcRes : entity.IptcData.Id;

            cmd.CommandText = String.Format(
                "UPDATE picture SET directory = @directory, file_name = @file_name, exif_id = @exif_id, iptc_id = @iptc_id{0}{1} WHERE id = @id",
                entity.PhotographerId > 0 ? ", photographer_id = @photographer_id" : "", entity.Tags == null ? "" : ", tags = @tags");
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "directory", Value = entity.Directory, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "file_name", Value = entity.FileName, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "exif_id", Value = exifId, DbType = DbType.Int32 });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "iptc_id", Value = iptcId, DbType = DbType.Int32 });
            if (entity.PhotographerId > 0) cmd.Parameters.Add(new SqliteParameter { ParameterName = "photographer_id", Value = entity.PhotographerId, DbType = DbType.Int32});
            if (entity.Tags != null) cmd.Parameters.Add(new SqliteParameter { ParameterName = "tags", Value = entity.Tags, DbType = DbType.String});
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = entity.Id, DbType = DbType.Int32 });

            cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
        }

        public void Delete(Picture entity)
        {
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
            };
            
            cmd.CommandText = "DELETE FROM picture WHERE id = @id";
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = entity.Id, DbType = DbType.Int32 });

            cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch

            if (entity.ExifDataId > 0)
            {
                cmd.CommandText = "DELETE FROM exif_data WHERE id = @id";
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = entity.ExifDataId, DbType = DbType.Int32 });
                cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
            }
            
            if (entity.IptcDataId > 0)
            {
                cmd.CommandText = "DELETE FROM iptc_data WHERE id = @id";
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = entity.IptcDataId, DbType = DbType.Int32 });
                cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
            }
        }

        /// <summary>
        ///    Deletes Picture with given ID in Database
        /// </summary>
        /// <param name="id">
        ///    ID of Picture entry that should be deleted
        /// </param>
        public void Delete(int id)
        {
            if (id <= 0) return;

            Delete(Get(id));
        }

        /// <summary>
        ///     Adds or updates the Exif data with given Exif object.
        /// </summary>
        /// <param name="exif">
        ///     exif object that contains the corresponding ID
        /// </param>
        /// <returns>
        ///    -1: Given exif data is null
        ///     0: Data was updated
        ///    >0: Data was inserted at returned id
        /// </returns>
        private int AddOrUpdateExif(ExifData exif)
        {
            if (exif == null) return -1;
            
            using var cmd = new SqliteCommand
            {
                Connection = _connection
            };

            if (exif.Id > 0) // exif has a valid id
            {
                cmd.CommandText = @"UPDATE exif_data SET exif_version = @exif_version, make = @make, model = @model, orientation = @orientation, datetime_original = @datetime_original WHERE id = @id";

                cmd.Parameters.Add(new SqliteParameter { ParameterName = "exif_version", Value = String.IsNullOrEmpty(exif.ExifVersion) ? DBNull.Value : (object) exif.ExifVersion, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "make", Value = String.IsNullOrEmpty(exif.Make) ? DBNull.Value : (object) exif.Make, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "model", Value = String.IsNullOrEmpty(exif.Model) ? DBNull.Value : (object) exif.Model, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "orientation", Value = String.IsNullOrEmpty(exif.Orientation) ? DBNull.Value : (object) exif.Orientation, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "datetime_original", Value = exif.DateTimeOriginal, DbType = DbType.DateTime });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = exif.Id, DbType = DbType.Int32 });

                try
                {
                    cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                return 0;
            }
            else // no id, so insert
            {
                cmd.CommandText = "INSERT INTO exif_data (exif_version, make, model, orientation, datetime_original) VALUES (@exif_version, @make, @model, @orientation, @datetime_original)";

                cmd.Parameters.Add(new SqliteParameter { ParameterName = "exif_version", Value = String.IsNullOrEmpty(exif.ExifVersion) ? DBNull.Value : (object) exif.ExifVersion, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "make", Value = String.IsNullOrEmpty(exif.Make) ? DBNull.Value : (object) exif.Make, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "model", Value = String.IsNullOrEmpty(exif.Model) ? DBNull.Value : (object) exif.Model, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "orientation", Value = String.IsNullOrEmpty(exif.Orientation) ? DBNull.Value : (object) exif.Orientation, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "datetime_original", Value = exif.DateTimeOriginal, DbType = DbType.DateTime });
            
                cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch

                cmd.CommandText = "SELECT last_insert_rowid() FROM exif_data";
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
        }
        
        /// <summary>
        ///     Adds or updates the iptc data with given iptc object in Database
        /// </summary>
        /// <param name="iptc">
        ///     iptc object that contains the corresponding ID
        /// </param>
        /// <returns>
        ///    -1: Given iptc data is null
        ///     0: Data was updated
        ///    >0: Data was inserted at returned id
        /// </returns>
        private int AddOrUpdateIptc(IptcData iptc)
        {
            if (iptc == null) return -1;
            
            using var cmd = new SqliteCommand
            {
                Connection = _connection
            };

            if (iptc.Id > 0) // exif has a valid id
            {
                cmd.CommandText = @"UPDATE iptc_data SET title = @title, key_words = @key_words, width = @width, height = @height, creators = @creators 
                                    WHERE id = @id";

                cmd.Parameters.Add(new SqliteParameter { ParameterName = "title", Value = String.IsNullOrEmpty(iptc.Title) ? DBNull.Value : (object) iptc.Title, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "key_words", Value = String.IsNullOrEmpty(iptc.KeyWords) ? DBNull.Value : (object) iptc.KeyWords, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "width", Value = iptc.Width, DbType = DbType.Int32 });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "height", Value = iptc.Height, DbType = DbType.Int32 });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "creators", Value = String.IsNullOrEmpty(iptc.Creators) ? DBNull.Value : (object) iptc.Creators, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = iptc.Id, DbType = DbType.Int32 });
            
                cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
                return 0;
            }
            else // no id, so insert
            {
                cmd.CommandText = "INSERT INTO iptc_data (title, key_words, width, height, creators) VALUES (@title, @key_words, @width, @height, @creators)";

                cmd.Parameters.Add(new SqliteParameter { ParameterName = "title", Value = String.IsNullOrEmpty(iptc.Title) ? DBNull.Value : (object) iptc.Title, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "key_words", Value = String.IsNullOrEmpty(iptc.KeyWords) ? DBNull.Value : (object) iptc.KeyWords, DbType = DbType.String });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "width", Value = iptc.Width, DbType = DbType.Int32 });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "height", Value = iptc.Height, DbType = DbType.Int32 });
                cmd.Parameters.Add(new SqliteParameter { ParameterName = "creators", Value = String.IsNullOrEmpty(iptc.Creators) ? DBNull.Value : (object) iptc.Creators, DbType = DbType.String });
                
                cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
                
                cmd.CommandText = "SELECT last_insert_rowid() FROM iptc_data";
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
        }

        /// <summary>
        ///     Loads Exif data out of the Database
        /// </summary>
        /// <param name="id">
        ///     ID of exif data that should be fetched
        /// </param>
        /// <returns>
        ///     Exif object that was fetched from Database
        /// </returns>
        private ExifData FetchExifData(int id)
        {
            ExifData data = null;

            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = @"SELECT id, exif_version, make, model, orientation, datetime_original FROM exif_data
                                WHERE id = @id LIMIT 1"
            };
            
            cmd.Parameters.Add(new SqliteParameter
            {
                ParameterName = "id",
                Value = id,
                DbType = DbType.Int32
            });

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                data = new ExifData
                {
                    Id = reader.GetInt32(0),
                    ExifVersion = reader.IsDBNull(1) ? null : reader.GetString(1),
                    Make = reader.IsDBNull(2) ? null : reader.GetString(2),
                    Model = reader.IsDBNull(3) ? null : reader.GetString(3),
                    Orientation = reader.IsDBNull(4) ? null : reader.GetString(4),
                    DateTimeOriginal = reader.IsDBNull(5) ? DateTime.MinValue : reader.GetDateTime(5)
                };
            }

            reader.Close();

            return data;
        }
        
        /// <summary>
        ///     Loads Iptc data out of the Database
        /// </summary>
        /// <param name="id">
        ///     ID of Iptc data that should be fetched
        /// </param>
        /// <returns>
        ///     Iptc object that was fetched from Database
        /// </returns>
        private IptcData FetchIptcData(int id)
        {
            IptcData data = null;

            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = @"SELECT id, title, key_words, width, height, creators FROM iptc_data
                                WHERE id = @id LIMIT 1"
            };
            
            cmd.Parameters.Add(new SqliteParameter
            {
                ParameterName = "id",
                Value = id,
                DbType = DbType.Int32
            });

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                data = new IptcData
                {
                    Id = reader.GetInt32(0),
                    Title = reader.IsDBNull(1) ? null : reader.GetString(1),
                    KeyWords = reader.IsDBNull(2) ? null : reader.GetString(2),
                    Width = reader.GetInt32(3),
                    Height = reader.GetInt32(4),
                    Creators = reader.IsDBNull(5) ? null : reader.GetString(5)
                };
            }

            reader.Close();

            return data;
        }

        /// <summary>
        /// Returns the first picture with the given directory and file_name
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Picture Find(Picture entity)
        {
            if (entity == null) return null;

            Picture result = null;
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "SELECT id, directory, file_name, iptc_id, exif_id, photographer_id, tags FROM picture WHERE directory = @directory AND file_name = @file_name LIMIT 1"
            };
            
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "directory", Value = entity.Directory, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "file_name", Value = entity.FileName, DbType = DbType.String });

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var picture = new Picture
                {
                    Id = reader.GetInt32(0),
                    Directory = reader.GetString(1),
                    FileName = reader.GetString(2),
                    IptcDataId = reader.GetInt32(3),
                    ExifDataId = reader.GetInt32(4),
                    PhotographerId = reader.GetInt32(5),
                    Tags = reader.GetString(6)
                };

                picture.ExifData = FetchExifData(picture.ExifDataId);
                picture.IptcData = FetchIptcData(picture.IptcDataId);

                result = picture;
                // break to return first result
                break;
            }

            reader.Close();
            return result;
        }
        
        /// <summary>
        ///     Finds all Pictures that are in the same directory as given picture
        /// </summary>
        /// <param name="entity">
        ///    entity of which to read the search directory
        /// </param>
        /// <returns>
        ///    List of all Pictures that have been found in directory
        /// </returns>
        public IEnumerable<Picture> FindMany(Picture entity)
        {
            if (entity == null) return null;

            List<Picture> results = new List<Picture>();
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "SELECT id, directory, file_name, iptc_id, exif_id, photographer_id, tags FROM picture WHERE directory = @directory"
            };
            
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "directory", Value = entity.Directory, DbType = DbType.String });

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var picture = new Picture
                {
                    Id = reader.GetInt32(0),
                    Directory = reader.GetString(1),
                    FileName = reader.GetString(2),
                    IptcDataId = reader.GetInt32(3),
                    ExifDataId = reader.GetInt32(4),
                    PhotographerId = reader.GetInt32(5),
                    Tags = reader.GetString(6)
                };

                picture.ExifData = FetchExifData(picture.ExifDataId);
                picture.IptcData = FetchIptcData(picture.IptcDataId);

                results.Add(picture);
            }

            reader.Close();
            return results;
        }
    }
}