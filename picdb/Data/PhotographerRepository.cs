using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using Microsoft.Data.Sqlite;
using PicDb.Models;

namespace PicDb.Data
{
    public class PhotographerRepository : IRepository<Photographer>
    {
        private readonly SqliteConnection _connection;

        public PhotographerRepository()
        {
            var settings = ConfigurationManager.ConnectionStrings[PicDbConfig.DbConnectionKey];
            var connectionString = settings.ConnectionString;
            _connection = new SqliteConnection(connectionString);
            
            _connection.Open();
        }

        public Photographer Get(int id)
        {
            if (id <= 0) return null;

            Photographer result = null;
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "SELECT id, first_name, last_name, birthdate, notes FROM photographer WHERE id = @id LIMIT 1"
            };
            
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = id, DbType = DbType.Int32 });

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var photographer = new Photographer
                {
                    Id = reader.GetInt32(0),
                    FirstName = reader.GetString(1),
                    LastName = reader.GetString(2),
                    Birthdate = reader.GetDateTime(3),
                    Notes = reader.GetString(4)
                };

                result = photographer;
            }

            reader.Close();
            return result;
        }

        public IEnumerable<Photographer> GetAll()
        {
            List<Photographer> photographers = new List<Photographer>();
            
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "SELECT id, first_name, last_name, birthdate, notes FROM photographer"
            };

            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                photographers.Add(new Photographer
                {
                    Id = reader.GetInt32(0),
                    FirstName = reader.GetString(1),
                    LastName = reader.GetString(2),
                    Birthdate = reader.GetDateTime(3),
                    Notes = reader.GetString(4)
                });
            }
            reader.Close();
            
            return photographers;
        }

        public Photographer Add(Photographer entity)
        {
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = @"INSERT INTO photographer (first_name, last_name, birthdate, notes) 
                                VALUES (@first_name, @last_name, @birthdate, @notes)"
            };

            cmd.Parameters.Add(new SqliteParameter { ParameterName = "first_name", Value = entity.FirstName, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "last_name", Value = entity.LastName, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "birthdate", Value = entity.Birthdate, DbType = DbType.Date });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "notes", Value = entity.Notes ?? "", DbType = DbType.String });

            cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
            
            cmd.CommandText = "SELECT last_insert_rowid() FROM photographer";
            entity.Id =  Convert.ToInt32(cmd.ExecuteScalar());

            return entity;
        }

        public void Update(Photographer entity)
        {
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = @"UPDATE photographer SET first_name = @first_name, last_name = @last_name, 
                                birthdate = @birthdate, notes = @notes WHERE id = @id"
            };

            cmd.Parameters.Add(new SqliteParameter { ParameterName = "first_name", Value = entity.FirstName, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "last_name", Value = entity.LastName, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "birthdate", Value = entity.Birthdate, DbType = DbType.Date });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "notes", Value = entity.Notes, DbType = DbType.String });
            cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = entity.Id, DbType = DbType.Int32 });

            cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
        }
        
        public void Delete(Photographer entity)
        {
            if (entity != null) Delete(entity.Id);
        }
        
        public void Delete(int id)
        {
            using var piccmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "UPDATE picture SET photographer_id = 0 WHERE photographer_id = @id"
            };

            piccmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = id, DbType = DbType.Int32 });

            piccmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
            
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = "DELETE FROM photographer WHERE id = @id"
            };

            cmd.Parameters.Add(new SqliteParameter { ParameterName = "id", Value = id, DbType = DbType.Int32 });

            cmd.ExecuteNonQuery(); // TODO: might want to move this into a try catch
        }
    }
}