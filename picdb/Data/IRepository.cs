using System.Collections.Generic;

namespace PicDb.Data
{
    /// <summary>
    /// Provides data fetching/manipulation functionality for the given entity.
    /// </summary>
    /// <typeparam name="T">Db entity</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Retrieves a single entity identified by its ID.
        /// </summary>
        /// <param name="id">Primary key (id) of the desired entity</param>
        /// <returns>
        ///    T: Entity with Id = given id param
        ///    null: Entity was not found
        /// </returns>
        T Get(int id);
        
        /// <summary>
        /// Retrieves an enumerable collection of all entities.
        /// </summary>
        /// <returns>Enumerable of all entities</returns>
        IEnumerable<T> GetAll();
        
        /// <summary>
        /// Add an entity to the collection.
        /// </summary>
        /// <param name="entity">Entity to add</param>
        /// <returns>Added entity with adjusted primary key (ID)</returns>
        T Add(T entity);
        
        /// <summary>
        /// Updates an entity in the collection.
        /// </summary>
        /// <param name="entity">Entity to update with the given entities properties (searches collection by comparing ID)</param>
        void Update(T entity);
        
        /// <summary>
        /// Deletes an entity from the collection.
        /// </summary>
        /// <param name="entity">Entity to be deleted (searches collection by comparing ID)</param>
        void Delete(T entity);
        
        /// <summary>
        /// Deletes an entity from the collection.
        /// </summary>
        /// <param name="id">Id of the entity to be deleted</param>
        void Delete(int id);
    }
}