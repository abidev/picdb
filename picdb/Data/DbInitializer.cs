using System;
using System.Configuration;
using System.Data.Common;
using System.IO;
using Microsoft.Data.Sqlite;
using PicDb.Models;
using Splat;

namespace PicDb.Data
{
    /// <summary>
    /// Creates db structure if db or a table does not exist.
    /// </summary>
    public class DbInitializer
    {
        private readonly SqliteConnection _connection;
        
        /// <summary>
        /// Creates data directory, opens a SQLite connection and initializes db.
        /// </summary>
        public DbInitializer()
        {
            var settings = ConfigurationManager.ConnectionStrings[PicDbConfig.DbConnectionKey];
            var dbConnectionString = settings.ConnectionString;
            _connection = new SqliteConnection(dbConnectionString);
            
            // Create data directory
            if (!Directory.Exists("data")) Directory.CreateDirectory("data");
            
            _connection.Open();
            
            InitializeDb();
        }

        private void InitializeDb()
        {
            CreateTableIfNotExists(
                "photographer",
                @"
                    CREATE TABLE photographer(
                          id INTEGER PRIMARY KEY,
                          first_name TEXT,
                          last_name TEXT,
                          birthdate DATE,
                          notes TEXT
                    )"
            );
            
            CreateTableIfNotExists(
                "exif_data",
                @"
                    CREATE TABLE exif_data(
                          id INTEGER PRIMARY KEY,
                          exif_version TEXT,
                          make TEXT,
                          model TEXT,
                          orientation TEXT,
                          datetime_original DATETIME,
                          picture_id INTEGER
                    )"
            );
            
            CreateTableIfNotExists(
                "iptc_data",
                @"
                    CREATE TABLE iptc_data(
                          id INTEGER PRIMARY KEY,
                          title TEXT,
                          key_words TEXT,
                          width INTEGER,
                          height INTEGER,
                          creators TEXT,
                          picture_id INTEGER
                    )"
            );
            
            CreateTableIfNotExists(
                "picture",
                @"
                    CREATE TABLE picture(
                          id INTEGER PRIMARY KEY,
                          directory TEXT,
                          file_name TEXT,
                          tags TEXT,
                          iptc_id INTEGER,
                          exif_id INTEGER,
                          photographer_id INTEGER,
                          FOREIGN KEY (iptc_id) REFERENCES iptc_data(id),
                          FOREIGN KEY (exif_id) REFERENCES exif_data(id)
                    )"
            );
        }

        private void CreateTableIfNotExists(string tableName, string createTableString)
        {
            // Query check if table exists
            using var cmd = new SqliteCommand
            {
                Connection = _connection,
                CommandText = $"SELECT count(name) FROM sqlite_master WHERE type='table' AND name='{tableName}'"
            };

            // Returns 1 (if table exists) or 0 (if table does not exist)
            using SqliteDataReader reader = cmd.ExecuteReader();

            var exists = false;
            while (reader.Read())
            {
                exists = reader.GetBoolean(0);
            }
            reader.Close();

            if (exists) return;
            
            // Create table if it does not exist
            cmd.CommandText = createTableString;
            cmd.ExecuteNonQuery();
        }
    }
}