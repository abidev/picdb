using System.Configuration;
using PicDb.BusinessLogic;
using PicDb.Data;
using PicDb.Models;
using PicDb.ViewModels;
using PicDb.ViewModels.TagsViewModels;
using PicDb.Views;
using ReactiveUI;
using Serilog;
using Splat;
using Splat.Serilog;
using ILogger = Serilog.ILogger;

namespace PicDb
{
    public class AppBootstrapper
    {
        public AppBootstrapper()
        {
            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File("log.txt", rollingInterval: RollingInterval.Day, shared: true)
                .WriteTo.Console()
                .CreateLogger();
            Locator.CurrentMutable.UseSerilogFullLogger();

            Locator.CurrentMutable.RegisterConstant(new DbInitializer());

            Locator.CurrentMutable.Register(() => new PictureView(), typeof(IViewFor<PictureViewModel>));
            Locator.CurrentMutable.Register(() => new PhotographerView(), typeof(IViewFor<PhotographerViewModel>));
            
            // PHOTOGRAPHER PAGE VIEW MODELS
            Locator.CurrentMutable.Register(() => new CreatePhotographerView(), typeof(IViewFor<CreatePhotographerViewModel>));
            Locator.CurrentMutable.Register(() => new EditPhotographerView(), typeof(IViewFor<EditPhotographerViewModel>));
            Locator.CurrentMutable.Register(() => new ShowPhotographerView(), typeof(IViewFor<ShowPhotographerViewModel>));
            Locator.CurrentMutable.Register(() => new ListPhotographersView(), typeof(IViewFor<ListPhotographersViewModel>));
            
            // PICTURE PAGE VIEW MODELS
            Locator.CurrentMutable.Register(() => new PictureLargeView(), typeof(IViewFor<PictureLargeViewModel>));
            Locator.CurrentMutable.Register(() => new PictureInfoView(), typeof(IViewFor<PictureInfoViewModel>));
            Locator.CurrentMutable.Register(() => new ShowPictureInfoView(), typeof(IViewFor<ShowPictureInfoViewModel>));
            Locator.CurrentMutable.Register(() => new EditPictureInfoView(), typeof(IViewFor<EditPictureInfoViewModel>));
            Locator.CurrentMutable.Register(() => new SearchView(), typeof(IViewFor<SearchViewModel>));
            
            Locator.CurrentMutable.RegisterLazySingleton(() => new PhotographerRepository(), typeof(IRepository<Photographer>));
            Locator.CurrentMutable.RegisterLazySingleton(() => new PictureRepository(), typeof(ISearchableRepository<Picture>));

            // This singleton should not by lazy, because it needs to initialize (constructor) on startup of the app
            Locator.CurrentMutable.RegisterConstant(new FileSystemManager());
            Locator.CurrentMutable.RegisterConstant(new PhotographerBusinessLogic());
            Locator.CurrentMutable.RegisterConstant(new PictureBusinessLogic());
        }
    }
}