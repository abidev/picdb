using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using MetadataExtractor;
using MetadataExtractor.Formats.Exif;
using MetadataExtractor.Formats.Iptc;
using PicDb.Data;
using PicDb.Models;
using Splat;
using Directory = System.IO.Directory;


namespace PicDb
{
    /// <summary>
    /// Handles initializing the repository from pictures in a directory. Handles updating repository in case of changes in the file system.
    /// </summary>
    public class FileSystemManager : IEnableLogger
    {
        private readonly ISearchableRepository<Picture> _pictureRepository;

        private FileSystemWatcher _watcher;
        
        /// <summary>
        /// Emits when files are changed in the file system.
        /// </summary>
        public event EventHandler<FileChangedEventArgs> FilesChanged;
        
        /// <summary>
        /// Emits when a new directory is selected.
        /// </summary>
        public event EventHandler<string> DirectoryChanged;
        
        private static readonly List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
        
        /// <summary>
        /// Holds the path to the currently selected directory.
        /// </summary>
        public string CurrentDirectory { get; private set; }

        public FileSystemManager()
        {
            _pictureRepository = Locator.Current.GetService<ISearchableRepository<Picture>>();

            this.Log().Info($"Starting {typeof(FileSystemManager)}...");

            // Initialize current directory to directory from config
            var initPath = ConfigurationManager.AppSettings[PicDbConfig.CurrentDirectoryKey];
            if (!String.IsNullOrEmpty(initPath))
            {
                ParseDirectory(initPath);
            }
        }

        /// <summary>
        /// Changes the current directory and processes pictures in the new directory.
        /// </summary>
        /// <param name="path">Path of the new directory</param>
        /// <returns>DirectoryParseResult indication the processing success</returns>
        public DirectoryParseResult ParseDirectory(string path)
        {
            if (path == CurrentDirectory)
            {
                return DirectoryParseResult.Success;
            }
            
            this.Log().Info($"Current directory changed to {path}");

            if (!Directory.Exists(path))
            {
                return DirectoryParseResult.NotFound;
            }

            CurrentDirectory = path;
            // Save new directory to config
            AddOrUpdateCurrentDirectoryWithSettings(path);
            // Setup file watcher to react to file system changes
            SetupFileSystemWatcher(path);
            DirectoryChanged?.Invoke(this, CurrentDirectory);
            
            return ReadFiles(path);
        }

        /// <summary>
        /// Updates or creates CurrentDirectory config key
        /// </summary>
        /// <param name="path">New value for CurrentDirectory</param>
        private void AddOrUpdateCurrentDirectoryWithSettings(string path)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);  
                var settings = configFile.AppSettings.Settings;  
                if (settings[PicDbConfig.CurrentDirectoryKey] == null)  
                {  
                    settings.Add(PicDbConfig.CurrentDirectoryKey, path);  
                }  
                else  
                {  
                    settings[PicDbConfig.CurrentDirectoryKey].Value = path;  
                }
                
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException e)
            {
                this.Log().Error($"Error writing app settings: {e.Message}");
            }
        }

        /// <summary>
        /// Reads files within a directory and syncs them with the repository.
        /// </summary>
        /// <param name="path">New directory path</param>
        /// <returns></returns>
        private DirectoryParseResult ReadFiles(string path)
        {
            var files = Directory.EnumerateFiles(path);
            foreach (var file in files)
            {
                SyncFileWithRepo(file);
            }
            
            // TODO: should pics be deleted from repo if they are no longer on disk?
            OnFileChanged(new FileChangedEventArgs {Path = path, ChangeType = ChangeType.DirectoryChanged});
            
            return DirectoryParseResult.Success;
        }

        /// <summary>
        /// Synchronizes a picture file with the repository.
        /// </summary>
        /// <param name="filePath">Path of the file to sync</param>
        /// <param name="oldFilePath">Optional old file path, if file path has changed</param>
        /// <exception cref="ImageProcessingException"></exception>
        private void SyncFileWithRepo(string filePath, string oldFilePath = null)
        {
            var fileName = filePath.Replace(CurrentDirectory, "").Replace(Path.DirectorySeparatorChar.ToString(), "");
            var oldFileName = oldFilePath?.Replace(CurrentDirectory, "").Replace(Path.DirectorySeparatorChar.ToString(), "");
            IReadOnlyList<MetadataExtractor.Directory> metadata;
            try
            {
                if (!ImageExtensions.Contains(Path.GetExtension(fileName).ToUpper()))
                {
                    throw new ImageProcessingException("Invalid file extension");
                }
                metadata = ImageMetadataReader.ReadMetadata(filePath);
            }
            catch (ImageProcessingException)
            {
                // Happens when a non-image file is read
                return;
            }
            catch (IndexOutOfRangeException)
            {
                metadata = null;
            }
            catch (IOException)
            {
                // Occurs when picture is opened by another application
                metadata = null;
            }
            
            var picture = new Picture
            {
                Directory = CurrentDirectory,
                FileName = oldFileName ?? fileName, // to search by old file name first
                ExifData = ReadFileExifData(metadata),
                IptcData = ReadFileIptcData(metadata)
            };
            var searchResult = _pictureRepository.Find(picture);
            picture.FileName = fileName;

            if (searchResult == null)
            {
                picture = _pictureRepository.Add(picture);
            }
            else
            {
                picture.Id = searchResult.Id;
                // Prioritize values from the repository
                picture.ExifData = new ExifData
                {
                    Id = searchResult.ExifData.Id,
                    ExifVersion = searchResult.ExifData?.ExifVersion ?? picture.ExifData?.ExifVersion,
                    Make = searchResult.ExifData?.Make ?? picture.ExifData?.Make,
                    Model = searchResult.ExifData?.Model ?? picture.ExifData?.Model,
                    Orientation = searchResult.ExifData?.Orientation ?? picture.ExifData?.Orientation,
                    DateTimeOriginal = searchResult.ExifData.DateTimeOriginal > DateTime.MinValue ? searchResult.ExifData.DateTimeOriginal : picture.ExifData.DateTimeOriginal
                };
                picture.IptcData = new IptcData
                {
                    Id = searchResult.IptcData.Id,
                    Title = searchResult.IptcData?.Title ?? picture.IptcData?.Title,
                    KeyWords = searchResult.IptcData?.KeyWords ?? picture.IptcData?.KeyWords,
                    Creators = searchResult.IptcData?.Creators ?? picture.IptcData?.Creators,
                    Height = searchResult.IptcData.Height > 0 ? searchResult.IptcData.Height : picture.IptcData.Height,
                    Width = searchResult.IptcData.Width > 0 ? searchResult.IptcData.Width : picture.IptcData.Width

                };
                _pictureRepository.Update(picture);
            }

            // this.Log().Info($"Synchronized picture with repository (id: {picture?.Id})");
        }
        
        /// <summary>
        /// Removes the given file from the repo.
        /// </summary>
        /// <param name="filePath">Path of the file to remove</param>
        private void RemoveFileFromRepo(string filePath)
        {
            var fileName = filePath.Replace(CurrentDirectory, "").Replace(Path.DirectorySeparatorChar.ToString(), "");
            var picture = new Picture
            {
                Directory = CurrentDirectory,
                FileName = fileName
            };
            var searchResult = _pictureRepository.Find(picture);
            
            _pictureRepository.Delete(searchResult);
            
            this.Log().Info($"Removed file from repository: {filePath}");
        }
        
        private ExifData ReadFileExifData(IReadOnlyList<MetadataExtractor.Directory> metadata)
        {
            if (metadata == null) return null;
            
            var dirs = metadata.OfType<ExifDirectoryBase>();

            var data = new ExifData();
            
            // Loop over exif data directories
            foreach (var subIfdDirectory in dirs)
            {
                var dateTimeOriginalString = subIfdDirectory.GetDescription(ExifDirectoryBase.TagDateTimeOriginal);
                DateTime.TryParseExact(dateTimeOriginalString, "yyyy:MM:dd HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out var dateTimeOriginal);

                if (data.DateTimeOriginal == DateTime.MinValue) data.DateTimeOriginal = dateTimeOriginal;
                data.ExifVersion ??= subIfdDirectory.GetDescription(ExifDirectoryBase.TagExifVersion);
                data.Make ??= subIfdDirectory.GetDescription(ExifDirectoryBase.TagMake);
                data.Model ??= subIfdDirectory.GetDescription(ExifDirectoryBase.TagModel);
                data.Orientation ??= subIfdDirectory.GetDescription(ExifDirectoryBase.TagOrientation);
            }

            return data;
        }
        
        private IptcData ReadFileIptcData(IReadOnlyList<MetadataExtractor.Directory> metadata)
        {
            var iptcDirectory = metadata?.OfType<IptcDirectory>()
                .FirstOrDefault();

            var data = new IptcData
            { // TODO: move to more relevant tags
                Title = iptcDirectory?.GetDescription(IptcDirectory.TagByLineTitle),
                KeyWords = iptcDirectory?.GetDescription(IptcDirectory.TagKeywords),
                Width = 0,
                Height = 0,
                Creators = ""
            };
            
            return data;
        }

        private void SetupFileSystemWatcher(string path)
        {
            _watcher = new FileSystemWatcher
            {
                Path = path,
                NotifyFilter = NotifyFilters.Size | NotifyFilters.FileName,
                Filter = "*.*"
            };

            //_watcher.Changed += OnFileSystemChanged;
            _watcher.Created += OnFileSystemCreated;
            _watcher.Deleted += OnFileSystemDeleted;
            _watcher.Renamed += OnFileSystemRenamed;

            _watcher.EnableRaisingEvents = true;
        }

        private void OnFileSystemChanged(object source, FileSystemEventArgs eventArgs)
        {   // could react to exif/iptc changes here...
            //TODO Debounce event.

            this.Log().Info($"File Changed: {eventArgs.FullPath} {eventArgs.ChangeType}");
            
            OnFileChanged(new FileChangedEventArgs
            {
                Path = eventArgs.FullPath,
                ChangeType = ChangeType.Changed
            });
        }
        
        private void OnFileSystemCreated(object source, FileSystemEventArgs eventArgs)
        {
            // needed to ignore all non image files
            if (ImageExtensions.Contains(Path.GetExtension(eventArgs.Name)?.ToUpper()))
            {
                this.Log().Info($"File Added: {eventArgs.FullPath}");
                SyncFileWithRepo(eventArgs.FullPath);
            
                OnFileChanged(new FileChangedEventArgs()
                {
                    Path = eventArgs.FullPath,
                    ChangeType = ChangeType.Created
                });
            }
        }
        
        private void OnFileSystemDeleted(object source, FileSystemEventArgs eventArgs)
        {
            if (ImageExtensions.Contains(Path.GetExtension(eventArgs.Name)?.ToUpper()))
            {
                this.Log().Info($"File Deleted: {eventArgs.FullPath} {eventArgs.ChangeType}");
                RemoveFileFromRepo(eventArgs.FullPath);

                OnFileChanged(new FileChangedEventArgs()
                {
                    Path = eventArgs.FullPath,
                    ChangeType = ChangeType.Deleted
                });
            }
        }
        
        private void OnFileSystemRenamed(object source, RenamedEventArgs eventArgs)
        {
            if (ImageExtensions.Contains(Path.GetExtension(eventArgs.Name)?.ToUpper()))
            {
                this.Log().Info($"File Renamed: {eventArgs.FullPath} {eventArgs.ChangeType}");
                SyncFileWithRepo(eventArgs.FullPath, eventArgs.OldFullPath);

                OnFileChanged(new FileChangedEventArgs()
                {
                    Path = eventArgs.FullPath,
                    ChangeType = ChangeType.Renamed
                });
            }
        }
        
        //Eventhandler to talk to BusinessLogic
        protected virtual void OnFileChanged(FileChangedEventArgs eventArgs)
        {
            EventHandler<FileChangedEventArgs> handler = FilesChanged;
            handler?.Invoke(this, eventArgs);
        }
    }
    
    /// <summary>
    /// Represents possible outcomes of parsing a directory for picture files.
    /// </summary>
    public enum DirectoryParseResult
    {
        NotFound,
        PermissionDenied,
        Success
    }

    /// <summary>
    /// Arguments used when file system change occurs.
    /// </summary>
    public class FileChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Path of the effected file.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// ChangeType
        /// <see cref="ChangeType"/>
        /// </summary>
        public ChangeType ChangeType {get; set;}
    }

    /// <summary>
    /// Represents possible changes of a file.
    /// </summary>
    public enum ChangeType
    {
        Deleted,
        Renamed,
        Created,
        Changed,
        DirectoryChanged
    }
}