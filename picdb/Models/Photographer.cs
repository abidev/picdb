using System;

namespace PicDb.Models
{
    public class Photographer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Notes { get; set; }
    }
}