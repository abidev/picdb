using System;

using System.IO;

namespace PicDb.Models
{
    public class Picture
    {
        public int Id { get; set; }
        public string Directory { get; set; }
        public string FileName { get; set; }
        public string Tags { get; set; }
        
        public int IptcDataId { get; set; }
        public IptcData IptcData { get; set; }
        public int ExifDataId { get; set; }
        public ExifData ExifData { get; set; }
        public int PhotographerId { get; set; }
        public Photographer Photographer { get; set; }

        public string GetAbsolutePath()
        {
            return Directory + Path.DirectorySeparatorChar + FileName;
        }
    }
}