using System;

namespace PicDb.Models
{
    public class ExifData
    {
        public int Id { get; set; }
        public string ExifVersion { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Orientation { get; set; }
        public DateTime DateTimeOriginal { get; set; }
    }
}