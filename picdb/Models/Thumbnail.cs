﻿using System;
using Avalonia;
using Avalonia.Media.Imaging;
using Avalonia.Visuals.Media.Imaging;
using ReactiveUI;

namespace PicDb.Models
{
    public class Thumbnail : ReactiveObject
    {
        public int Id { get; set; }
        public Bitmap ThumbnailPic { get; set; }

        public Thumbnail(int id)
        {
            Id = id;
            ThumbnailPic = null;
        }

        public Thumbnail(int id, string path, int compressionRate)
        {
            Id = id;
            try
            {
                ThumbnailPic = new Bitmap(path);

                double heightNew = ThumbnailPic.Size.Height * (1 - compressionRate / 100.0);
                double widthNew = ThumbnailPic.Size.Width * (1 - compressionRate / 100.0);
                
                PixelSize pixelSize = new PixelSize((int)widthNew, (int)heightNew);

                ThumbnailPic = ThumbnailPic.CreateScaledBitmap(pixelSize, BitmapInterpolationMode.MediumQuality);
            }
            catch (NullReferenceException)
            {
                //File is not a bitmap.
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Id = 0;
            }
        }
    }
}