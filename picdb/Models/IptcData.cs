using System.Collections.Generic;

namespace PicDb.Models
{
    public class IptcData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string KeyWords { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Creators { get; set; }
    }
}