var indexSectionsWithContent =
{
  0: "acdefgilmopstuv",
  1: "acdefilmpstv",
  2: "p",
  3: "acdfgiopsu",
  4: "c",
  5: "cd",
  6: "cpt",
  7: "dfpst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "properties",
  7: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Properties",
  7: "Events"
};

