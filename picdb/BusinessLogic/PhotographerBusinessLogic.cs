using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using PicDb.Data;
using PicDb.Dialogs;
using PicDb.Models;
using PicDb.ViewModels.Dialogs;
using Splat;

namespace PicDb.BusinessLogic
{
    /// <summary>
    /// Manages photographer entities within the application.
    /// </summary>
    public class PhotographerBusinessLogic: IEnableLogger
    {
        private readonly IRepository<Photographer> _photographerRepository;

        /// <summary>
        /// Emits when a photographer is added to the repository.
        /// </summary>
        public event EventHandler<PhotographerEventArgs> PhotographerAdded;
        
        /// <summary>
        /// Emits when the currently selected photographers changes.
        /// </summary>
        public event EventHandler<PhotographerEventArgs> PhotographerToShowChanged;
        
        /// <summary>
        /// Emits when photographer editing is started.
        /// </summary>
        public event EventHandler<PhotographerEventArgs> PhotographerEditClick;
        
        /// <summary>
        /// Emits when a photographer is updated to the repository.
        /// </summary>
        public event EventHandler<PhotographerEventArgs> PhotographerUpdated;
        
        /// <summary>
        /// Emits when a photographer is deleted from the repository.
        /// </summary>
        public event EventHandler<PhotographerEventArgs> PhotographerDeleted;

        private Photographer _photographerToShow;
        /// <summary>
        /// Holds the currently selected photographer. 
        /// </summary>
        public Photographer PhotographerToShow
        {
            get => _photographerToShow;
            private set
            {
                _photographerToShow = value;
                this.Log().Info($"Selected new photographer (id: {value?.Id})");
            } 
        }
        
        public PhotographerBusinessLogic()
        {
            _photographerRepository = Locator.Current.GetService<IRepository<Photographer>>();

            // Preselects the first photographer of the list
            PhotographerToShow = _photographerRepository.GetAll()?.FirstOrDefault();
        }

        /// <summary>
        /// Changes the currently selected photographer to the photographer with the given id.
        /// </summary>
        /// <param name="photographerId">Photographer id to change to</param>
        public void ChangePhotographerToShow(int photographerId)
        {
            var photographer = _photographerRepository.Get(photographerId);
            
            PhotographerToShow = photographer;
            PhotographerToShowChanged?.Invoke(this, new PhotographerEventArgs{ Photographer = photographer });
        }

        /// <summary>
        /// Fires the PhotographerEditClick event.
        /// </summary>
        public void StartPhotographerEdit()
        {
            PhotographerEditClick?.Invoke(this, new PhotographerEventArgs{ Photographer = PhotographerToShow });
        }

        /// <summary>
        /// Fetches the photographer with the specified id.
        /// </summary>
        /// <param name="id">Id of the photographer to fetch</param>
        /// <returns>
        ///     Photographer: If a photographer was found
        ///     null: If there is no photographer with this id
        /// </returns>
        public Photographer Get(int id)
        {
            return _photographerRepository.Get(id);
        }
 
        /// <summary>
        /// Fetches a list of all photographers in the repository.
        /// </summary>
        /// <returns>List of photographers</returns>
        public IEnumerable<Photographer> GetAll()
        {
            return _photographerRepository.GetAll();
        }

        /// <summary>
        /// Adds a valid photographer to the repository. Selects the newly added photographer.
        /// Fires PhotographerToShowChanged and PhotographerAdded events.
        /// </summary>
        /// <param name="photographer">Photographer to add</param>
        /// <returns>
        ///     true: Validation successful, photographer added
        ///     false: Validation failed, no changes to repository
        /// </returns>
        public bool Add(Photographer photographer)
        {
            var validationResult = ValidatePhotographer(photographer);
            if (!validationResult) return false;
            
            var res = _photographerRepository.Add(photographer);
            
            PhotographerToShow = photographer;
            PhotographerToShowChanged?.Invoke(this, new PhotographerEventArgs{ Photographer = photographer });
            
            PhotographerAdded?.Invoke(this, new PhotographerEventArgs{ Photographer = photographer });
            
            this.Log().Info($"Added new photographer (id: {res?.Id})");

            return true;
        }

        /// <summary>
        /// Updates a photographer with valid data. Selects updated photographer.
        /// Fires PhotographerToShowChanged and PhotographerUpdated.
        /// </summary>
        /// <param name="photographer">Photographer to update (with new data)</param>
        /// <returns>
        ///     true: Validation successful, photographer updated
        ///     false: Validation failed, no changes to repository
        /// </returns>
        public bool Update(Photographer photographer)
        {
            var validationResult = ValidatePhotographer(photographer);
            if (!validationResult) return false;
            
            _photographerRepository.Update(photographer);
            
            PhotographerToShow = photographer;
            PhotographerToShowChanged?.Invoke(this, new PhotographerEventArgs{ Photographer = photographer });
            
            PhotographerUpdated?.Invoke(this, new PhotographerEventArgs{ Photographer = photographer });

            this.Log().Info($"Updated photographer (id: {photographer?.Id})");
            
            return true;
        }

        /// <summary>
        /// Deletes a photographer from the repository. Changes selection to first in list.
        /// Fires PhotographerToShowChanged and PhotographerDeleted.
        /// </summary>
        /// <param name="photographer">Photographer to delete</param>
        /// <returns></returns>
        public void Delete(Photographer photographer)
        {
            _photographerRepository.Delete(photographer);

            PhotographerToShow = _photographerRepository.GetAll().FirstOrDefault();
            PhotographerToShowChanged?.Invoke(this, new PhotographerEventArgs{ Photographer = PhotographerToShow });
            
            PhotographerDeleted?.Invoke(this, new PhotographerEventArgs{ Photographer = photographer });
            
            this.Log().Info($"Deleted photographer (id: {photographer?.Id})");
        }

        /// <summary>
        /// Validates a photographer's data.
        /// </summary>
        /// <param name="photographer">Photographer to validate</param>
        /// <returns>
        ///     true: Validation successful
        ///     false: Validation failed
        /// </returns>
        private bool ValidatePhotographer(Photographer photographer)
        {
            if (String.IsNullOrEmpty(photographer?.FirstName) || photographer.FirstName.Length > 100 ||
                String.IsNullOrEmpty(photographer.LastName) || photographer.LastName.Length > 50 ||
                photographer.Birthdate > DateTime.Today || String.IsNullOrEmpty(photographer.Notes))
            {
                return false;
            }

            return true;
        }
    }

    /// <summary>
    /// Represents a change involving a photographer.
    /// </summary>
    public class PhotographerEventArgs : EventArgs
    {
        /// <summary>
        /// Photographer that was modified (created, updated, deleted, selected,...)
        /// </summary>
        public Photographer Photographer { get; set; }
    }
}