﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Avalonia.Media.Imaging;
using DynamicData;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PicDb.Data;
using PicDb.Models;
using Serilog;
using Splat;
using Path = System.IO.Path;

namespace PicDb.BusinessLogic
{
    /// <summary>
    /// Manages picture entities within the application.
    /// </summary>
    public class PictureBusinessLogic : IBusinessLogic, IEnableLogger
    {
        private readonly FileSystemManager _fileSystemManager;
        private readonly ISearchableRepository<Picture> _pictureRepository;
        private readonly PhotographerBusinessLogic _photographerBusinessLogic;
        
        private Dictionary<string, int> Tags { get; set; }
        
        /// <summary>
        /// Holds a list of all pictures in the repository.
        /// </summary>
        private List<Picture> Pictures { get; set; }
        
        /// <summary>
        /// Holds a list of all pictures in the currently selected directory.
        /// </summary>
        private List<Picture> CurrentPictures { get; set; }
        
        /// <summary>
        /// Holds a list of filtered pictures (if a valid search string is present)
        /// </summary>
        private List<Picture> FilteredPictures { get; set; }
        
        /// <summary>
        /// Holds a list of thumbnails for all pictures of the currently selected directory.
        /// </summary>
        private List<Thumbnail> CurrentThumbnails { get; set; }

        // TODO: move to config
        /// <summary>
        /// Compression rate used to create thumbnails from full size pictures.
        /// </summary>
        public const int CompressionRate = 75;
        
        /// <summary>
        /// Emits when the selected thumbnail changes.
        /// </summary>
        public event EventHandler<ThumbnailsChangedEventArgs> ThumbnailsChanged;

        /// <summary>
        /// Emits when picture info editing was initialized.
        /// </summary>
        public event EventHandler<PictureEventArgs> PictureEditInfoClick;

        /// <summary>
        /// Emits when the currently selected picture changes.
        /// </summary>
        public event EventHandler<PictureEventArgs> PictureToShowChanged;

        private Picture _pictureToShow;
        /// <summary>
        /// Currently selected picture. (To be shown in picture large view and data edit views)
        /// </summary>
        public Picture PictureToShow
        {
            get => _pictureToShow;
            private set
            {
                _pictureToShow = value;
                this.Log().Info($"Selected new picture (id: {value?.Id})");
            }
        }

        private string _currentSearchString;
        /// <summary>
        /// Holds the search string which was used for the currently filtered pictures.
        /// </summary>
        public string CurrentSearchString
        {
            get => _currentSearchString;
            private set
            {
                _currentSearchString = value;
                SearchStringChanged?.Invoke(this, value);
            }
        }
        
        /// <summary>
        /// Emits when the current search string changes. (this means a new filtered list of pictures is available)
        /// </summary>
        public event EventHandler<string> SearchStringChanged;

        public PictureBusinessLogic()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            _fileSystemManager = Locator.Current.GetService<FileSystemManager>();
            _pictureRepository = Locator.Current.GetService<ISearchableRepository<Picture>>();
            _photographerBusinessLogic = Locator.Current.GetService<PhotographerBusinessLogic>();

            // Caches pictures in the in-memory local lists
            Pictures = (List<Picture>) _pictureRepository?.GetAll();
            Tags = new Dictionary<string, int>();
            CurrentThumbnails = new List<Thumbnail>();
        }
        
        /// <summary>
        /// Initializes the list of thumbnails for CurrentPictures.
        /// </summary>
        public void Initialize()
        {
            Tags = ExtractTags(Pictures);
            GetCurrentPictures(_fileSystemManager.CurrentDirectory);
            
            // Preselects the first picture in the list
            var firstPic = CurrentPictures.FirstOrDefault();
            if (firstPic != null)
                ChangeLargeImageToShow(firstPic.Id);
            
            if(CurrentPictures != null)
                GenerateThumbnails(CompressionRate);
            
            _fileSystemManager.FilesChanged += NotifyFilesChanged;
            
            OnThumbnailChanged(new ThumbnailsChangedEventArgs
            {
                Thumbnails = CurrentThumbnails,
                ChangeType = ChangeType.DirectoryChanged
            });
        }

        /// <summary>
        /// Adjusts cached lists according to changes in the file system and fires events indicating the change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void NotifyFilesChanged(object sender, FileChangedEventArgs args)
        {
            this.Log().Info($"Detected file system change (type: {args?.ChangeType})");
            
            string directory;
            string fileName;

            Picture picture;
            Thumbnail thumbnail;
            switch (args.ChangeType)
            {
                case ChangeType.Deleted:
                    
                    int id = CurrentPictures?.Find(x => x.GetAbsolutePath() == args.Path)?.Id ?? 0;
                    CurrentPictures?.RemoveAll(x => x.GetAbsolutePath() == args.Path);

                    // Emit thumbnail deleted if the thumbnail was found in the local list
                    if (id > 0)
                    {
                        OnThumbnailChanged(new ThumbnailsChangedEventArgs
                        {
                            Thumbnails = new List<Thumbnail> {new Thumbnail(id)},
                            ChangeType = ChangeType.Deleted
                        });
                    }
                    break;
                
                case ChangeType.Renamed:
                    // A file rename has no effect on the previewed thumbnail
                    break;
                
                case ChangeType.Created:
                    //a new file was added/created to/in the directory
                    directory = args.Path.Substring(0, args.Path.LastIndexOf(Path.DirectorySeparatorChar));
                    fileName = args.Path.Remove(0, args.Path.LastIndexOf(Path.DirectorySeparatorChar)+1);
                    picture = _pictureRepository.Find(new Picture {Directory = directory, FileName = fileName});
                    
                    // Add the new picture to locally cached list
                    CurrentPictures.Add(picture);
                    // Create thumbnail and add it to the list
                    thumbnail = new Thumbnail(picture.Id, picture.GetAbsolutePath(), CompressionRate);
                    CurrentThumbnails.Add(thumbnail);
                    
                    // Emit thumbnail added event
                    OnThumbnailChanged(new ThumbnailsChangedEventArgs
                    {
                        Thumbnails = new List<Thumbnail> {thumbnail},
                        ChangeType = ChangeType.Created
                    });
                    break;
                
                case ChangeType.Changed:
                    // Replace all changed images in the locally cached list
                    CurrentPictures.RemoveAll(x => x.GetAbsolutePath() == args.Path );
                    directory = args.Path.Substring(0, args.Path.LastIndexOf(Path.DirectorySeparatorChar));
                    fileName = args.Path.Remove(0, args.Path.LastIndexOf(Path.DirectorySeparatorChar)+1);
                    picture = _pictureRepository.Find(new Picture{Directory = directory, FileName = fileName});
                    CurrentPictures.Add(picture);

                    // Replace all thumbnails of changed pictures in the local thumbnail list
                    thumbnail = new Thumbnail(picture.Id, picture.GetAbsolutePath(), CompressionRate);
                    CurrentThumbnails.RemoveAll(x => x.Id == picture.Id);
                    CurrentThumbnails.Add(thumbnail);
                    
                    // Emit thumbnail changed event
                    OnThumbnailChanged(new ThumbnailsChangedEventArgs
                    {
                        Thumbnails = new List<Thumbnail> {thumbnail},
                        ChangeType = ChangeType.Changed
                    });
                    break;
                case ChangeType.DirectoryChanged:
                    // Clear local caches
                    CurrentPictures.Clear();
                    CurrentThumbnails.Clear();
                    
                    // Rebuild cache from pictures in the newly selected directory
                    CurrentPictures = (List<Picture>) _pictureRepository.FindMany(new Picture {Directory = args.Path});
                    GenerateThumbnails(CompressionRate);
            
                    // Emit directory changed event
                    OnThumbnailChanged(new ThumbnailsChangedEventArgs
                    {
                        Thumbnails = CurrentThumbnails,
                        ChangeType = ChangeType.DirectoryChanged
                    });
                    
                    // Select first picture in the list
                    var firstPicture = CurrentPictures?.FirstOrDefault();
                    if (firstPicture != null)
                        ChangeLargeImageToShow(firstPicture.Id);
                    break;
            }
        }
        
        /// <summary>
        /// Rebuilds local cache of current directory images from the repository.
        /// </summary>
        /// <param name="path">Path of the desired directory</param>
        private void GetCurrentPictures(string path)
        {
            CurrentPictures = (List<Picture>) _pictureRepository.FindMany(new Picture{Directory = path});
        }

        /// <summary>
        /// Rebuilds thumbnail cache from current directory images.
        /// </summary>
        /// <param name="compressionRate">Compression rate for generating thumbnails</param>
        /// <param name="picList">If set, thumbnails will be generated from this list instead of CurrentPictures</param>
        private void GenerateThumbnails(int compressionRate, List<Picture> picList = null)
        {
            var pics = CurrentPictures;
            if (picList != null) pics = picList;
            
            foreach (var pic in pics)
            {
                // If the thumbnail does not exist in the cache, generate and store it
                if (CurrentThumbnails.All(i => i.Id != pic.Id))
                {
                    Thumbnail thumb = new Thumbnail(pic.Id, pic.GetAbsolutePath(), compressionRate);
                    CurrentThumbnails.Add(thumb);
                }
            }
        }
        
        private Dictionary<string, int> ExtractTags(List<Picture> pictures)
        {
            var extractedTags = new Dictionary<string, int>();
            if (pictures != null)
            {
                foreach (var pic in pictures)
                {
                    if (pic.Tags != null)
                    {
                        var tags = new List<string>(pic.Tags.Split(';').Select(x => x.Trim()).ToArray().Distinct()
                            .ToList());

                        foreach (var tag in tags)
                        {
                            if (tag != string.Empty)
                            {
                                if (extractedTags.ContainsKey(tag))
                                {
                                    extractedTags[tag]++;
                                }
                                else
                                {
                                    extractedTags.Add(tag, 1);
                                }
                            }
                        }
                    }
                }
            }
            return extractedTags;
        }
        
        private void UpdateTags(Picture pictureToUpdateTags)
        {
            if (Pictures == null)
            {
                return;
            }
            var newTags= pictureToUpdateTags.Tags.Split(';').Select(x => x.Trim()).ToArray().Distinct().ToList();
            var oldPic = Pictures.First(x => x.Id == pictureToUpdateTags.Id);
            var oldTags = new List<string>(oldPic.Tags.Split(';').Select(x => x.Trim()).ToArray().Distinct().ToList());

            if (newTags == oldTags)
            {
                return;
            }

            var differenceNewToOld = newTags.Except(oldTags).ToList();
            var differenceOldToNew = oldTags.Except(newTags).ToList();

            foreach (var deletedTag in differenceOldToNew)
            {
                Tags[deletedTag]--;
                if (Tags[deletedTag] <= 0)
                {
                    Tags.Remove(deletedTag);
                }
            }
            
            foreach (var tag in differenceNewToOld)
            {
                if (tag != string.Empty)
                {
                    if (Tags.ContainsKey(tag))
                    {
                        Tags[tag]++;
                    }
                    else
                    {
                        Tags.Add(tag, 1);
                    }
                }
            }
        }

        /// <summary>
        /// Prints list of all tags to the current directory
        /// </summary>
        /// <param name="includeEmptyTags">Sets if count of empty tags is included in list</param>
        /// <param name="tagInfoPrintType">Sets of which pictures the Tag info should be printed</param>
        public void PrintTagInfo(bool includeEmptyTags, TagInfoPrintType tagInfoPrintType)
        {
            Dictionary<string, int> tagsToPrint;
            string headingText = "Tag Report for ";
            string filename = "tagReport";
            string directory;
            
            switch (tagInfoPrintType)
            {
                case TagInfoPrintType.AllPictures:
                    tagsToPrint = Tags.ToDictionary(k => k.Key.ToString(), k => k.Value);
                    headingText += "all Pictures.";
                    //TODO maybe use a file dialog for selecting destination DIR
                    directory = CurrentPictures.First().Directory;
                    filename += "_allPic";
                    if (includeEmptyTags)
                    {
                        tagsToPrint.Add("empty Tags", 0);
                        tagsToPrint["empty Tags"] = Pictures.Count(x => x.Tags == "");
                    }
                    break;
                case TagInfoPrintType.ThisPicture:
                    tagsToPrint = ExtractTags(new List<Picture>{PictureToShow});
                    headingText += PictureToShow.FileName;
                    directory = PictureToShow.Directory;
                    filename += $"_{Path.GetFileNameWithoutExtension(PictureToShow.FileName)}";
                    if (includeEmptyTags)
                    {
                        tagsToPrint.Add("empty Tags", 0);
                        tagsToPrint["empty Tags"] = Pictures.Count(x => x.Tags == "");
                    }
                    break;
                case TagInfoPrintType.CurrentPicturesInDirectory:
                    tagsToPrint = ExtractTags(CurrentPictures);
                    headingText += "all Pictures in current Directory";
                    directory = CurrentPictures.First().Directory;
                    var tempDir = CurrentPictures.First().Directory;
                    tempDir += Path.DirectorySeparatorChar;
                    
                    filename += $"_{Path.GetFileName(Path.GetDirectoryName(tempDir))}";
                    
                    if (includeEmptyTags)
                    {
                        tagsToPrint.Add("empty Tags", 0);
                        tagsToPrint["empty Tags"] = Pictures.Count(x => x.Tags == "");
                    }
                    break;
                default:
                    tagsToPrint = Tags.ToDictionary(k => k.Key.ToString(), k => k.Value);
                    headingText += "all Pictures.";
                    directory = CurrentPictures.First().Directory;
                    filename += "_allPic";
                    if (includeEmptyTags)
                    {
                        tagsToPrint.Add("empty Tags", 0);
                        tagsToPrint["empty Tags"] = Pictures.Count(x => x.Tags == "");
                    }
                    break;
            }
            
            // Setup document
            var document = new Document();
            document.UseCmykColor = true;
            
            var section = document.AddSection();

            var paragraph = section.AddParagraph();
            paragraph.Format.Font.Color = Colors.DarkRed;

            paragraph.AddFormattedText(headingText, TextFormat.Bold);
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Size = 35;
            paragraph.AddLineBreak();
            paragraph.Format.KeepWithNext = true;

            var spacePara = section.AddParagraph();
            spacePara.Format.SpaceBefore = "0.25cm";
            spacePara.Format.Borders.Bottom = new Border() {Width = "2pt", Color = Colors.PaleVioletRed};
            spacePara.Format.KeepWithNext = true;
            spacePara.Format.SpaceAfter = "0.75cm";
            
            var photographerInfoTable = new Table();
            photographerInfoTable.Format.Alignment = ParagraphAlignment.Left;
            photographerInfoTable.Rows.Height = 25;
            
            // Setup columns
            photographerInfoTable.AddColumn("7.0cm");
            photographerInfoTable.AddColumn();

            // Setup rows
            Row photographerInfoTableRowHeader = photographerInfoTable.AddRow();
            photographerInfoTableRowHeader.HeadingFormat = true;
            photographerInfoTableRowHeader.Shading.Color = Colors.IndianRed;
            
            photographerInfoTableRowHeader.Cells[0].AddParagraph("Tag Name");
            photographerInfoTableRowHeader.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            photographerInfoTableRowHeader.Cells[1].AddParagraph("count");

            Row photographerInfoTableRowData;
            foreach (var currentTag in tagsToPrint)
            {
                photographerInfoTableRowData = photographerInfoTable.AddRow();
                photographerInfoTableRowData.Cells[0].AddParagraph($"{currentTag.Key}");
                photographerInfoTableRowData.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                photographerInfoTableRowData.Cells[1].AddParagraph($"{currentTag.Value.ToString()}");
            }
            
            section.Add(photographerInfoTable);
            
            // Handle creation of Reports folder
            var subPath = $"{directory}{Path.DirectorySeparatorChar.ToString()}Reports";
            
            if(!Directory.Exists(subPath))
                Directory.CreateDirectory(subPath);
            
            // Setup renderer
            var pdfRenderer = new PdfDocumentRenderer(true);
            pdfRenderer.Document = document;
            
            pdfRenderer.RenderDocument();

            // Give filename and save to newly created PDF file.
            filename = $"{subPath}{Path.DirectorySeparatorChar.ToString()}{filename}.pdf";
            pdfRenderer.PdfDocument.Save(filename);
            
            Log.Logger.Information($"Printed {headingText} to {filename}");
        }

        /// <summary>
        /// Returns a list of the currently cache thumbnails.
        /// </summary>
        /// <returns>List of thumbnails</returns>
        public IEnumerable<Thumbnail> GetCurrentThumbnails()
        {
            return CurrentThumbnails;
        }
        
        /// <summary>
        /// Returns the first picture for the local picture cache.
        /// </summary>
        /// <param name="picId">Id of the picture to be returned</param>
        /// <returns>Picture</returns>
        public Picture GetPicture(int picId)
        {
            return Pictures?.FirstOrDefault(pic => pic.Id == picId);
        }

        /// <summary>
        /// Notifies the file system manager of a directory change.
        /// </summary>
        /// <param name="path">New directory path</param>
        public void ParseDirectory(string path)
        {
            _fileSystemManager.ParseDirectory(path);
        }
        
        /// <summary>
        /// Emits the ThumbnailsChanged event with the given arguments.
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnThumbnailChanged(ThumbnailsChangedEventArgs args)
        {
            if (!args.Thumbnails.Any()) return;
            
            ThumbnailsChanged?.Invoke(this, args);
        }

        /// <summary>
        /// Selects a new picture and emits PictureToShowChanged.
        /// </summary>
        /// <param name="picId">Id of the picture to select</param>
        public void ChangeLargeImageToShow(int picId)
        {
            PictureToShow = CurrentPictures.Find(x => x.Id == picId);
            PictureToShowChanged?.Invoke(this, new PictureEventArgs {Picture = PictureToShow});
        }

        /// <summary>
        /// Creates a bitmap from the given picture.
        /// </summary>
        /// <param name="picture">Picture to create bitmap from</param>
        /// <returns>Bitmap</returns>
        public Bitmap GetPictureBitmap(Picture picture)
        {
            if (picture?.Directory == null || picture?.FileName == null)
            {
                if (picture?.Id > 0)
                {
                    picture = GetPicture(picture.Id);
                }
            }

            try
            {
                return new Bitmap(picture?.GetAbsolutePath());
            }
            catch (Exception e) // TODO: change to specific exception
            {
                Pictures?.Remove(picture);
                // Gets triggered if file thats loaded is not an image
                return null;
            }
        }

        /// <summary>
        /// Updates the given picture with repository and updates local caches. Selects the updated picture.
        /// Fires PictureToShowChanged event.
        /// </summary>
        /// <param name="picture"></param>
        public void Update(Picture picture)
        {
            _pictureRepository.Update(picture);
            UpdateTags(picture);

            PictureToShow = picture;
            PictureToShowChanged?.Invoke(this, new PictureEventArgs {Picture = PictureToShow});

            var pic = Pictures.FirstOrDefault(p => p.Id == picture.Id);
            var curPic = CurrentPictures.FirstOrDefault(p => p.Id == picture.Id);
            if (pic != null)
                Pictures.Replace(pic, picture);
            if (curPic != null)
                CurrentPictures.Replace(curPic, picture);
            
            this.Log().Info($"Updated picture (id: {picture?.Id})");
        }

        /// <summary>
        /// Fires PictureEditInfoClick event.
        /// </summary>
        public void StartPictureInfoEdit()
        {
            PictureEditInfoClick?.Invoke(this, new PictureEventArgs {Picture = PictureToShow});
        }

        /// <summary>
        /// Updates given ExifData with repository, updates local caches.
        /// </summary>
        /// <param name="exifData">Exif data to update</param>
        /// <returns>Not implemented</returns>
        public bool UpdateExifData(ExifData exifData)
        {
            var index = CurrentPictures.FindIndex(x => x.ExifDataId == exifData.Id);
            var picToEdit = CurrentPictures[index];
            
            picToEdit.ExifData = exifData;
            
            CurrentPictures[index] = picToEdit;
            _pictureRepository.Update(picToEdit);
            
            this.Log().Info($"Updated exif data (id: {exifData?.Id})");
            
            //Not optimal, Update should return true or false
            return true;
        }
        
        /// <summary>
        /// Updates given IptcData with repository, updates local caches.
        /// </summary>
        /// <param name="iptcData">Iptc data to update</param>
        /// <returns>Not implemented</returns>
        public bool UpdateIptcData(IptcData iptcData)
        {
            var index = CurrentPictures.FindIndex(x => x.ExifDataId == iptcData.Id);
            var picToEdit = CurrentPictures[index];
            
            picToEdit.IptcData = iptcData;
            
            CurrentPictures[index] = picToEdit;
            _pictureRepository.Update(picToEdit);
            
            this.Log().Info($"Updated iptc data (id: {iptcData?.Id})");
            
            //Not optimal, Update should return true or false
            return true;
        }

        /// <summary>
        /// Searches picture cache for given search string. The search is not case sensitive.
        /// </summary>
        /// <param name="searchString">Search string to search with</param>
        public void Search(string searchString)
        {
            // Only search if the search string has changed
            if (searchString == CurrentSearchString) return;

            // Search should not be case sensitive
            searchString = searchString.ToLower().Trim();
            CurrentSearchString = searchString;
            
            if (!String.IsNullOrEmpty(CurrentSearchString))
            {   // Search string is not empty, filter images
                // Lazy-load photographer entities into pictures if not present
                foreach (var picture in CurrentPictures)
                {
                    if (picture.Photographer == null && picture.PhotographerId > 0)
                    {
                        picture.Photographer = _photographerBusinessLogic.Get(picture.PhotographerId);
                    }
                }

                // Filter current pictures by tags, exif data, iptc data, photographer data
                var query = from pic in CurrentPictures
                                where pic.Tags.ToLower().Contains(searchString) || SearchExif(pic.ExifData, searchString) || 
                                      SearchIptc(pic.IptcData, searchString) || SearchPhotographer(pic.Photographer, searchString)
                                select pic;
                
                FilteredPictures = query.ToList();

                // Rebuild thumbnails from filtered list
                CurrentThumbnails.Clear();
                GenerateThumbnails(CompressionRate, FilteredPictures);
                
                // Emit directory change event to indicate complete thumbnail rebuild
                OnThumbnailChanged(new ThumbnailsChangedEventArgs
                {
                    Thumbnails = CurrentThumbnails,
                    ChangeType = ChangeType.DirectoryChanged
                });

                // Select the first picture in the filtered pictures list
                PictureToShow = FilteredPictures?.FirstOrDefault();
                PictureToShowChanged?.Invoke(this, new PictureEventArgs {Picture = PictureToShow});
            }
            else
            {   // Search string is empty, reset to all current directory images
                // Rebuild all thumbnails
                CurrentThumbnails.Clear();
                GenerateThumbnails(CompressionRate);
            
                // Emit directory change event to indicate complete thumbnail rebuild
                OnThumbnailChanged(new ThumbnailsChangedEventArgs
                {
                    Thumbnails = CurrentThumbnails,
                    ChangeType = ChangeType.DirectoryChanged
                });
                
                // Select the first picture in the current pictures list
                PictureToShow = CurrentPictures?.FirstOrDefault();
                PictureToShowChanged?.Invoke(this, new PictureEventArgs {Picture = PictureToShow});
            }
            
            this.Log().Info($"Finished preparing resulst for search: {searchString}");
        }

        private bool SearchExif(ExifData data, string searchString)
        {
            return data.Make != null && data.Make.ToLower().Contains(searchString) ||
                   data.Model != null && data.Model.ToLower().Contains(searchString) ||
                   data.ExifVersion != null && data.ExifVersion.ToLower().Contains(searchString) ||
                   data.Orientation != null && data.Orientation.ToLower().Contains(searchString) ||
                   data.DateTimeOriginal.ToString(CultureInfo.InvariantCulture).Contains(searchString);
        }
        
        private bool SearchIptc(IptcData data, string searchString)
        {
            return data.Title != null && data.Title.ToLower().Contains(searchString) || 
                   data.KeyWords != null && data.KeyWords.ToLower().Contains(searchString) ||
                   data.Width.ToString().Contains(searchString) || 
                   data.Height.ToString().Contains(searchString) ||
                   data.Creators != null && data.Creators.ToLower().Contains(searchString);

        }

        private bool SearchPhotographer(Photographer photographer, string searchString)
        {
            return photographer?.FirstName != null && photographer?.LastName != null &&
                   ($"{photographer.FirstName} {photographer.LastName}".ToLower().Contains(searchString) ||
                   photographer.Birthdate.ToString(CultureInfo.InvariantCulture).Contains(searchString) ||
                   photographer.Notes.ToLower().Contains(searchString));
        }

        /// <summary>
        /// Creates a pdf report of the given picture and saves it within the picture's directory.
        /// </summary>
        /// <param name="pictureToShow">Picture to generate report from</param>
        public void PrintPictureInfo(Picture pictureToShow)
        {
            // Setup document
            var document = new Document();
            document.UseCmykColor = true;

            var section = document.AddSection();

            var paragraph = section.AddParagraph();
            paragraph.Format.Font.Color = Colors.Blue;

            paragraph.AddFormattedText($"Report for \n{pictureToShow.FileName}", TextFormat.Bold);
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Size = 35;
            paragraph.AddLineBreak();
            paragraph.Format.KeepWithNext = true;

            var spacePara = section.AddParagraph();
            spacePara.Format.SpaceBefore = "0.25cm";
            spacePara.Format.Borders.Bottom = new Border() {Width = "2pt", Color = Colors.Aquamarine};
            spacePara.Format.KeepWithNext = true;

            // Add paragraph for image
            var imageParagraph = section.AddParagraph();
            imageParagraph.Format.SpaceBefore = "1.0cm";
            imageParagraph.Format.Alignment = ParagraphAlignment.Center;

            // Compress Image to save some space
            var thumb = CurrentThumbnails.Find(x => x.Id == pictureToShow.Id)?.ThumbnailPic;
            if (thumb != null)
            {
                string stringImage = Convert.ToBase64String(ImageToByte(thumb));
                string finalImage = "base64:" + stringImage;

                var imageInPdf = imageParagraph.AddImage(finalImage);

                imageInPdf.LockAspectRatio = true;
                imageInPdf.Height = "7cm";
                imageInPdf.Resolution = 0.8;
            }

        var photographerHeadingParagraph = section.AddParagraph();
            photographerHeadingParagraph.Format.SpaceBefore = "1.0cm";
            photographerHeadingParagraph.AddFormattedText("Photographer Info");
            photographerHeadingParagraph.Format.Alignment = ParagraphAlignment.Center;
            photographerHeadingParagraph.Format.Font.Size = 20;
            photographerHeadingParagraph.AddLineBreak();
            photographerHeadingParagraph.Format.SpaceAfter = "0.75cm";
            photographerHeadingParagraph.Format.KeepWithNext = true;
            
            var photographerInfoTable = new Table();
            photographerInfoTable.Format.Alignment = ParagraphAlignment.Left;
            photographerInfoTable.Rows.Height = 25;
            
            // Setup columns
            photographerInfoTable.AddColumn("3.0cm");
            photographerInfoTable.AddColumn();
            photographerInfoTable.AddColumn();
            photographerInfoTable.AddColumn();
            photographerInfoTable.AddColumn("5.0cm");

            // Setup rows
            Row photographerInfoTableRowHeader = photographerInfoTable.AddRow();
            photographerInfoTableRowHeader.HeadingFormat = true;
            photographerInfoTableRowHeader.Shading.Color = Colors.Aquamarine;
            
            photographerInfoTableRowHeader.Cells[0].AddParagraph("Photographer Id");
            photographerInfoTableRowHeader.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            photographerInfoTableRowHeader.Cells[1].AddParagraph("First Name");
            photographerInfoTableRowHeader.Cells[2].AddParagraph("Last Name");
            photographerInfoTableRowHeader.Cells[3].AddParagraph("Birthdate");
            photographerInfoTableRowHeader.Cells[4].AddParagraph("Notes");

            Row photographerInfoTableRowData = photographerInfoTable.AddRow();
            photographerInfoTableRowData.Cells[0].AddParagraph($"{pictureToShow.Photographer?.Id.ToString()}");
            photographerInfoTableRowData.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            photographerInfoTableRowData.Cells[1].AddParagraph($"{pictureToShow.Photographer?.FirstName}");
            photographerInfoTableRowData.Cells[2].AddParagraph($"{pictureToShow.Photographer?.LastName}");
            photographerInfoTableRowData.Cells[3].AddParagraph($"{pictureToShow.Photographer?.Birthdate.ToShortDateString()}");
            photographerInfoTableRowData.Cells[4].AddParagraph($"{pictureToShow.Photographer?.Notes}");
            
            section.Add(photographerInfoTable);
            
            // Create Table for EXIF Data
            var exifDataHeadingParagraph = section.AddParagraph();
            exifDataHeadingParagraph.Format.SpaceBefore = "1.0cm";
            exifDataHeadingParagraph.AddFormattedText("EXIF Data");
            exifDataHeadingParagraph.Format.Alignment = ParagraphAlignment.Center;
            exifDataHeadingParagraph.Format.Font.Size = 20;
            exifDataHeadingParagraph.AddLineBreak();
            exifDataHeadingParagraph.Format.SpaceAfter = "0.75cm";
            exifDataHeadingParagraph.Format.KeepWithNext = true;
            
            var exifDataInfoTable = new Table();
            exifDataInfoTable.Format.Alignment = ParagraphAlignment.Left;
            exifDataInfoTable.Rows.Height = 25;
            
            // Setup columns
            exifDataInfoTable.AddColumn("3.0cm");
            exifDataInfoTable.AddColumn();
            exifDataInfoTable.AddColumn();
            exifDataInfoTable.AddColumn();
            exifDataInfoTable.AddColumn();
            exifDataInfoTable.AddColumn();

            // Setup rows
            Row exifDataInfoTableRowHeader = exifDataInfoTable.AddRow();
            exifDataInfoTableRowHeader.HeadingFormat = true;
            exifDataInfoTableRowHeader.Shading.Color = Colors.Aquamarine;
            
            exifDataInfoTableRowHeader.Cells[0].AddParagraph("EXIF Data Id");
            exifDataInfoTableRowHeader.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            exifDataInfoTableRowHeader.Cells[1].AddParagraph("Version");
            exifDataInfoTableRowHeader.Cells[2].AddParagraph("Make");
            exifDataInfoTableRowHeader.Cells[3].AddParagraph("Model");
            exifDataInfoTableRowHeader.Cells[4].AddParagraph("Orientation");
            exifDataInfoTableRowHeader.Cells[5].AddParagraph("Date");

            Row exifDataInfoTableRowData = exifDataInfoTable.AddRow();
            exifDataInfoTableRowData.Cells[0].AddParagraph($"{pictureToShow.ExifData?.Id.ToString()}");
            exifDataInfoTableRowData.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            exifDataInfoTableRowData.Cells[1].AddParagraph($"{pictureToShow.ExifData?.ExifVersion}");
            exifDataInfoTableRowData.Cells[2].AddParagraph($"{pictureToShow.ExifData?.Make}");
            exifDataInfoTableRowData.Cells[3].AddParagraph($"{pictureToShow.ExifData?.Model}");
            exifDataInfoTableRowData.Cells[4].AddParagraph($"{pictureToShow.ExifData?.Orientation}");
            exifDataInfoTableRowData.Cells[5].AddParagraph($"{pictureToShow.ExifData?.DateTimeOriginal.ToShortDateString()}");
            
            section.Add(exifDataInfoTable);
            
            // Create section for IPTC data
            var iptcDataHeadingParagraph = section.AddParagraph();
            iptcDataHeadingParagraph.Format.SpaceBefore = "1.0cm";
            iptcDataHeadingParagraph.AddFormattedText("IPTC Data");
            iptcDataHeadingParagraph.Format.Alignment = ParagraphAlignment.Center;
            iptcDataHeadingParagraph.Format.Font.Size = 20;
            iptcDataHeadingParagraph.AddLineBreak();
            iptcDataHeadingParagraph.Format.SpaceAfter = "0.75cm";
            iptcDataHeadingParagraph.Format.KeepWithNext = true;
            
            var iptcDataInfoTable = new Table();
            iptcDataInfoTable.Format.Alignment = ParagraphAlignment.Left;
            iptcDataInfoTable.Rows.Height = 25;
            
            // Setup columns
            iptcDataInfoTable.AddColumn("3.0cm");
            iptcDataInfoTable.AddColumn();
            iptcDataInfoTable.AddColumn();
            iptcDataInfoTable.AddColumn();
            iptcDataInfoTable.AddColumn();
            iptcDataInfoTable.AddColumn();

            // Setup rows
            Row iptcDataInfoTableRowHeader = iptcDataInfoTable.AddRow();
            iptcDataInfoTableRowHeader.HeadingFormat = true;
            iptcDataInfoTableRowHeader.Shading.Color = Colors.Aquamarine;
            
            iptcDataInfoTableRowHeader.Cells[0].AddParagraph("IPTC Data Id");
            iptcDataInfoTableRowHeader.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            iptcDataInfoTableRowHeader.Cells[1].AddParagraph("Title");
            iptcDataInfoTableRowHeader.Cells[2].AddParagraph("KeyWords");
            iptcDataInfoTableRowHeader.Cells[3].AddParagraph("Width");
            iptcDataInfoTableRowHeader.Cells[4].AddParagraph("Height");
            iptcDataInfoTableRowHeader.Cells[5].AddParagraph("Creators");

            Row iptcDataInfoTableRowData = iptcDataInfoTable.AddRow();
            iptcDataInfoTableRowData.Cells[0].AddParagraph($"{pictureToShow.IptcData?.Id.ToString()}");
            iptcDataInfoTableRowData.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            iptcDataInfoTableRowData.Cells[1].AddParagraph($"{pictureToShow.IptcData?.Title}");
            iptcDataInfoTableRowData.Cells[2].AddParagraph($"{pictureToShow.IptcData?.KeyWords}");
            iptcDataInfoTableRowData.Cells[3].AddParagraph($"{pictureToShow.IptcData?.Width.ToString()} cm");
            iptcDataInfoTableRowData.Cells[4].AddParagraph($"{pictureToShow.IptcData?.Height.ToString()} cm");
            iptcDataInfoTableRowData.Cells[5].AddParagraph($"{pictureToShow.IptcData?.Creators}");
            
            section.Add(iptcDataInfoTable);
            
            // Handle creation of Reports folder
            var subPath = $"{pictureToShow.Directory}{Path.DirectorySeparatorChar.ToString()}Reports";

            bool exists = Directory.Exists(subPath);

            if(!exists)
                Directory.CreateDirectory(subPath);
            
            // Setup renderer
            var pdfRenderer = new PdfDocumentRenderer(true);
            pdfRenderer.Document = document;
            
            pdfRenderer.RenderDocument();
            
            // Give filename and save to newly created PDF file.
            string filename = $"{subPath}{Path.DirectorySeparatorChar.ToString()}{pictureToShow.FileName.Remove(pictureToShow.FileName.LastIndexOf('.'))}_report.pdf";
            pdfRenderer.PdfDocument.Save(filename);
            
            Log.Logger.Information("Printed Picture Info of {0} with Pic Id = {1} to {2}", pictureToShow.FileName, pictureToShow.Id, "DIR");
        }

        private static byte[] ImageToByte(IBitmap img)
        {
            using var ms = new MemoryStream();
            img?.Save(ms);
            return ms.ToArray();
        }
    }

    /// <summary>
    /// Represents a change involving a picture.
    /// </summary>
    public class PictureEventArgs : EventArgs
    {
        /// <summary>
        /// Picture that was modified (created, updated, deleted, selected,...)
        /// </summary>
        public Picture Picture { get; set; }
    }

    /// <summary>
    /// Represents a change involving a thumbnail.
    /// </summary>
    public class ThumbnailsChangedEventArgs : EventArgs
    {
        /// <summary>
        /// List of changed thumbnails.
        /// </summary>
        public List<Thumbnail> Thumbnails { get; set; }
        
        /// <summary>
        /// Indicates change type (see ChangeType enum).
        /// <see cref="ChangeType"/>
        /// </summary>
        public ChangeType ChangeType { get; set; }
    }

    public enum TagInfoPrintType
    {
        AllPictures,
        ThisPicture,
        CurrentPicturesInDirectory
    }
}