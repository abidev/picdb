using System.Collections.Generic;
using System.Linq;
using DynamicData;
using PicDb.BusinessLogic;
using PicDb.Data;
using PicDb.Models;

namespace picdb_tests
{
    public class MockPictureRepository: ISearchableRepository<Picture>
    {
        private readonly List<Picture> _pictures = new List<Picture>();
        private int _nextId = 1;
        
        public Picture Get(int id)
        {
            return _pictures.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Picture> GetAll()
        {
            return _pictures;
        }

        public Picture Add(Picture entity)
        {
            entity.Id = _nextId++;
            _pictures.Add(entity);
            return entity;
        }

        public void Update(Picture entity)
        {
            var picture = _pictures.FirstOrDefault(p => p.Id == entity.Id);
            if (picture != null)
            {
                _pictures.Replace(picture, entity);
            }
        }

        public void Delete(Picture entity)
        {
            var picture = _pictures.FirstOrDefault(p => p.Id == entity.Id);
            if (picture != null)
            {
                _pictures.Remove(picture);
            }
        }

        public void Delete(int id)
        {
            var picture = _pictures.FirstOrDefault(p => p.Id == id);
            if (picture != null)
            {
                _pictures.Remove(picture);
            }
        }

        public Picture Find(Picture entity)
        {
            return _pictures.FirstOrDefault(p => p.Id == entity.Id);
        }

        public IEnumerable<Picture> FindMany(Picture entity)
        {
            return _pictures.FindAll(p => p.Directory == entity.Directory);
        }
    }
}