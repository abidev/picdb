using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using NUnit.Framework;
using PicDb;
using PicDb.BusinessLogic;
using PicDb.Data;
using PicDb.Models;
using Splat;

namespace picdb_tests
{
    public class PictureBusinessLogicTests
    {
        private PictureBusinessLogic _pictureBusinessLogic;
        private static string _testFileDir;
        private static string _testFileDir2;
        private List<Picture> _pictures;

        [OneTimeSetUp]
        public void Setup()
        {
            _testFileDir = "testPics";
            _testFileDir2 = "testPics_2";
            
            Locator.CurrentMutable.RegisterLazySingleton(() => new MockPictureRepository(), typeof(ISearchableRepository<Picture>));
            Locator.CurrentMutable.RegisterLazySingleton(() => new MockPhotographerRepository(), typeof(IRepository<Photographer>));
            Locator.CurrentMutable.RegisterConstant(new FileSystemManager());

            _pictureBusinessLogic = new PictureBusinessLogic();
            
            _pictureBusinessLogic.Initialize();
            
            _pictureBusinessLogic.ParseDirectory(_testFileDir);
            
            _pictures = new List<Picture>();
            
            var thumbnails = _pictureBusinessLogic.GetCurrentThumbnails();
            
            foreach (var thumb in thumbnails)
            {
                Picture pic = _pictureBusinessLogic.GetPicture(thumb.Id);
                _pictures.Add(pic);
            }
            
            _pictureBusinessLogic.ChangeLargeImageToShow(_pictures.First().Id);
        }
        
        [TearDown]
        public void Dispose()
        {
            
        }
        
        [Test]
        public void GetCurrentThumbnails()
        {
            Assert.NotNull(_pictureBusinessLogic.GetCurrentThumbnails());
        }
        
        [Test]
        public void GetCurrentThumbnails_imageDir()
        {
            var thumbnails = _pictureBusinessLogic.GetCurrentThumbnails();
            Assert.NotNull(thumbnails);
            Assert.AreEqual( _pictures.Count(),thumbnails.Count());
        }
        
        [Test]
        public void ChangeLargeImageToShow_validId()
        {
            var picIdToChangeTo = _pictureBusinessLogic.GetCurrentThumbnails().First().Id;
            
            _pictureBusinessLogic.ChangeLargeImageToShow(picIdToChangeTo);
            
            var pic = _pictureBusinessLogic.GetPicture(_pictureBusinessLogic.PictureToShow.Id);
            Assert.NotNull(pic);
            Assert.AreEqual(picIdToChangeTo, pic.Id);
        }
        
        [Test]
        public void UpdatePicture()
        {
            var pic = _pictures.FirstOrDefault();
            pic.Tags = RandomString(5);
            pic.ExifData.Make = RandomString(10);
            pic.IptcData.Title = RandomString(15);
            pic.Photographer = new Photographer{Id = 1, FirstName = RandomString(10), LastName = RandomString(15)};
            
            _pictureBusinessLogic.Update(pic);
            _pictureBusinessLogic.ChangeLargeImageToShow(pic.Id);
            
            var picAfterUpdate = _pictureBusinessLogic.PictureToShow;
            
            Assert.AreEqual(pic.Tags, picAfterUpdate.Tags);
            Assert.AreEqual(pic.ExifData.Make, pic.ExifData.Make);
            Assert.AreEqual(pic.IptcData.Title, picAfterUpdate.IptcData.Title);
            Assert.AreEqual(pic.Photographer.FirstName, picAfterUpdate.Photographer.FirstName);
            Assert.AreEqual(pic.Photographer.LastName, picAfterUpdate.Photographer.LastName);
        }
        
        [Test]
        public void GetPicture_validId()
        {
            Assert.NotNull(_pictureBusinessLogic.GetPicture(1));
        }
        
        [Test]
        public void GetPicture_invalidId()
        {
            Assert.Null(_pictureBusinessLogic.GetPicture(-1));
        }

        [Test]
        public void ParseDirToFileSystemManager_sameDir()
        {
            _pictureBusinessLogic.ParseDirectory(_testFileDir);
            Assert.AreEqual(_pictures.Count(), _pictureBusinessLogic.GetCurrentThumbnails().Count());
        }
        
        [Test]
        public void ChangeLargeImageToShow_invalidId()
        {
            _pictureBusinessLogic.ChangeLargeImageToShow(-1);
            Assert.Null(_pictureBusinessLogic.PictureToShow);
        }
        
        
        //Report creation (PDF)
        [Test]
        public void PrintTagInfo_allPictures()
        {
            _pictureBusinessLogic.PrintTagInfo(true, TagInfoPrintType.AllPictures);

            var path = $"{_testFileDir}{Path.DirectorySeparatorChar}Reports{Path.DirectorySeparatorChar}tagReport_allPic.pdf";
            Assert.IsTrue(File.Exists(path));
        }
        
        [Test]
        public void PrintTagInfo_currentPicture()
        {
            _pictureBusinessLogic.PrintTagInfo(true, TagInfoPrintType.CurrentPicturesInDirectory);

            var testDir = _testFileDir + Path.DirectorySeparatorChar;
            
            var path = $"{_testFileDir}{Path.DirectorySeparatorChar}Reports{Path.DirectorySeparatorChar}tagReport_{Path.GetFileName(Path.GetDirectoryName(testDir))}.pdf";
            Assert.IsTrue(File.Exists(path));
        }
        
        [Test]
        public void PrintTagInfo_thisPicture()
        {
            _pictureBusinessLogic.PrintTagInfo(true, TagInfoPrintType.ThisPicture);

            var path = $"{_testFileDir}{Path.DirectorySeparatorChar}Reports{Path.DirectorySeparatorChar}tagReport_{Path.GetFileNameWithoutExtension(_pictureBusinessLogic.PictureToShow.FileName)}.pdf";
            Assert.IsTrue(File.Exists(path));
        }

        [Test]
        public void PrintPictureInfo()
        {
            _pictureBusinessLogic.ChangeLargeImageToShow(_pictures.First(x => x.Directory == _testFileDir).Id);
            _pictureBusinessLogic.PrintPictureInfo(_pictureBusinessLogic.PictureToShow);

            var path = $"{_testFileDir}{Path.DirectorySeparatorChar}Reports{Path.DirectorySeparatorChar}{Path.GetFileNameWithoutExtension(_pictureBusinessLogic.PictureToShow.FileName)}_report.pdf";
            Assert.IsTrue(File.Exists(path));
        }
        
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}