﻿using System;
using System.Linq;
using NUnit.Framework;
using PicDb;
using PicDb.BusinessLogic;
using PicDb.Data;
using PicDb.Models;
using Splat;

namespace picdb_tests
{
    [TestFixture]
    public class PhotographerBusinessLogicTests
    {
        private PhotographerBusinessLogic _photographerBusinessLogic;

        [SetUp]
        public void Setup()
        {
            Locator.CurrentMutable.RegisterLazySingleton(() => new MockPictureRepository(), typeof(ISearchableRepository<Picture>));
            Locator.CurrentMutable.RegisterLazySingleton(() => new MockPhotographerRepository(), typeof(IRepository<Photographer>));
            Locator.CurrentMutable.RegisterConstant(new FileSystemManager());
            
            _photographerBusinessLogic = new PhotographerBusinessLogic();
        }

        [Test]
        public void Get_should_return_photographer()
        {
            var entity = _photographerBusinessLogic.Get(1);

            Assert.That(entity, Is.Not.Null);
            
            Assert.That(entity.FirstName, Is.EqualTo("Max"));
        }

        [Test]
        public void GetAll_should_return_enumerable_of_photographers()
        {
            var entities = _photographerBusinessLogic.GetAll();
            
            Assert.That(entities, Is.Not.Null);
            
            Assert.That(entities.Count(), Is.GreaterThanOrEqualTo(2));
        }

        [Test]
        public void Add_should_add_photographer_to_repository()
        {
            var newPhotographer = new Photographer
            {
                FirstName = "Gerhard",
                LastName = "Genügend",
                Birthdate = DateTime.Today - TimeSpan.FromDays(8000),
                Notes = "notes"
            };

            var repoLength = _photographerBusinessLogic.GetAll().Count();

            _photographerBusinessLogic.Add(newPhotographer);

            var entites = _photographerBusinessLogic.GetAll();
            
            Assert.That(entites.Count(), Is.GreaterThan(repoLength));
        }

        [Test]
        public void Update_should_update_photographer_in_repository()
        {
            var entity = _photographerBusinessLogic.Get(1);

            var entityToUpdate = new Photographer
            {
                Id = entity.Id,
                FirstName = "Maximilian",
                LastName = entity.LastName,
                Birthdate = entity.Birthdate,
                Notes = entity.Notes
            };

            var result = _photographerBusinessLogic.Update(entityToUpdate);
            var updatedEntity = _photographerBusinessLogic.Get(1);
            
            Assert.That(updatedEntity.FirstName, Is.EqualTo("Maximilian"));
        }

        [Test]
        public void Delete_should_delete_photographer_from_repository()
        {
            var entity = _photographerBusinessLogic.Get(1);
            
            _photographerBusinessLogic.Delete(entity);

            var deletedEntity = _photographerBusinessLogic.Get(1);
            
            Assert.That(deletedEntity, Is.Null);
        }

        [Test]
        public void Add_should_not_add_invalid_photographer()
        {
            var newPhotographer = new Photographer
            {
                FirstName = null,
                LastName = null,
                Birthdate = DateTime.MinValue,
                Notes = null
            };

            var repoLength = _photographerBusinessLogic.GetAll().Count();

            _photographerBusinessLogic.Add(newPhotographer);

            var entites = _photographerBusinessLogic.GetAll();
            
            Assert.That(entites.Count(), Is.EqualTo(repoLength));
        }
        
        [Test]
        public void Add_should_return_invalid_for_too_long_firstName()
        {
            // Firstname has 101 characters
            var newPhotographer = new Photographer
            {
                FirstName = "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",
                LastName = "Genügend",
                Birthdate = DateTime.Today - TimeSpan.FromDays(8000),
                Notes = "notes"
            };

            var result = _photographerBusinessLogic.Add(newPhotographer);
            
            Assert.That(result, Is.False);
        }
        
        [Test]
        public void Add_should_return_invalid_for_too_long_lastName()
        {
            // Lastname has 51 characters
            var newPhotographer = new Photographer
            {
                FirstName = "Gerhard",
                LastName = "012345678901234567890123456789012345678901234567890",
                Birthdate = DateTime.Today - TimeSpan.FromDays(8000),
                Notes = "notes"
            };

            var result = _photographerBusinessLogic.Add(newPhotographer);
            
            Assert.That(result, Is.False);
        }
        
        [Test]
        public void Add_should_return_invalid_for_future_date()
        {
            // Date is in the future
            var newPhotographer = new Photographer
            {
                FirstName = "Gerhard",
                LastName = "Genügend",
                Birthdate = DateTime.Today + TimeSpan.FromDays(1),
                Notes = "notes"
            };

            var result = _photographerBusinessLogic.Add(newPhotographer);
            
            Assert.That(result, Is.False);
        }
        
        [Test]
        public void Update_should_return_validation_result()
        {
            var entity = _photographerBusinessLogic.Get(1);

            entity.Birthdate = DateTime.Now + TimeSpan.FromDays(1);

            var result = _photographerBusinessLogic.Update(entity);
            
            Assert.That(result, Is.False);
        }
        
        [Test]
        public void Update_should_not_update_repository_given_invalid_data()
        {
            var entity = _photographerBusinessLogic.Get(1);
            var originalLastName = entity.LastName;

            var entityToUpdate = new Photographer
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = "012345678901234567890123456789012345678901234567890",
                Birthdate = entity.Birthdate,
                Notes = entity.Notes
            };

            var result = _photographerBusinessLogic.Update(entityToUpdate);

            var updatedEntity = _photographerBusinessLogic.Get(1);
            
            Assert.That(result, Is.False);
            Assert.That(updatedEntity.LastName, Is.EqualTo(originalLastName));
        }
    }
}