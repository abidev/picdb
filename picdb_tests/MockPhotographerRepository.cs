using System;
using System.Collections.Generic;
using System.Linq;
using DynamicData;
using PicDb.Data;
using PicDb.Models;

namespace picdb_tests
{
    public class MockPhotographerRepository: IRepository<Photographer>
    {
        private readonly List<Photographer> _photographers = new List<Photographer>();
        private int _nextId = 1;

        public MockPhotographerRepository()
        {
            Add(new Photographer
            {
                FirstName = "Max",
                LastName = "Mustermann",
                Birthdate = DateTime.Now - TimeSpan.FromDays(10000),
                Notes = "Max notes"
            });
            Add(new Photographer
            {
                FirstName = "Gerlinde",
                LastName = "Musterfrau",
                Birthdate = DateTime.Now - TimeSpan.FromDays(15000),
                Notes = "Gerlinde notes"
            });
        }
        
        public Photographer Get(int id)
        {
            return _photographers.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Photographer> GetAll()
        {
            return _photographers;
        }

        public Photographer Add(Photographer entity)
        {
            entity.Id = _nextId++;
            _photographers.Add(entity);
            return entity;
        }

        public void Update(Photographer entity)
        {
            var photographer = _photographers.FirstOrDefault(p => p.Id == entity.Id);
            if (photographer != null)
            {
                _photographers.Replace(photographer, entity);
            }
        }

        public void Delete(Photographer entity)
        {
            var photographer = _photographers.FirstOrDefault(p => p.Id == entity.Id);
            if (photographer != null)
            {
                _photographers.Remove(photographer);
            }
        }

        public void Delete(int id)
        {
            var photographer = _photographers.FirstOrDefault(p => p.Id == id);
            if (photographer != null)
            {
                _photographers.Remove(photographer);
            }
        }
    }
}